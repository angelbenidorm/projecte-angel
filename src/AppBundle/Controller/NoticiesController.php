<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\noticiaType;
use AppBundle\Entity\noticia;

class NoticiesController extends DefaultController{
    //-------------------------------ZONA DE NOTICIES-----------------------------
    //AFEGIR NOTICIA
    public function afegirNoticiaAction(Request $request, $id){
        $em =$this->getDoctrine()->getManager();
        if(is_null($id)){
            $noticia=new noticia();
            $fecha = new \DateTime();
            $noticia->setFecha($fecha);
            $noticia->setUsuarios($this->getUser());
        }
        else{
            $noticia=$em->getRepository('AppBundle:noticia')->find($id);

            //controla que l'usuari que ha creat la noticia coincideixca amb l'usuari logejat o siga moderador
            if(!$this->isGranted('ROLE_MODERADOR')){
                if($this->getUser()->getUsername()!= $noticia->getUsuarios()->getUsername()){
                    $mensaje=array('error'=>'No tens permís per a editar aquesta noticia.');
                    return($this->render('AppBundle:Default:error.html.twig',$mensaje));
                }
            }
        }
        $form= $this->createForm(new noticiaType(), $noticia, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST') {

            $form->bind($request);
            if($form->isValid()){
                $noticia->setTitulo($form['titulo']->getData());
                $noticia->setTexto($form['texto']->getData());
                $noticia->setFuente($form['fuente']->getData());
                if (substr($form['url']->getData(), 0, 4) == 'http'){
                    $guardaUrl = explode('//',$form['url']->getData());
                    $noticia->setUrl($guardaUrl[1]);
                }else {
                    $noticia->setUrl($form['url']->getData());
                }
                $noticia->setUsuarios($this->getUser());

                $dir='bundles/AppBundle/banda/noticies/';
                if($form['imagenNoticia']->getData()){
                    $archivo=$form['imagenNoticia']->getData();
                    $nombre=time().$form['imagenNoticia']->getData()->getClientOriginalName();
                    $imageNova=$this->redimensionaImatge($archivo, $form['imagenNoticia']->getData()->getClientOriginalName());

                    //imagejpg() copia la image al directori
                    imagejpeg($imageNova, $dir.$nombre);
                    $noticia->setImagenNoticia($nombre);
                }
                else{
                    if($noticia->getImagenNoticia()==NULL){
                        $imagen='defaultNoticia.jpg';
                        $noticia->setImagenNoticia($imagen);
                    }
                }

                $em->persist($noticia);
                $em->flush();
                $params['id']=$noticia->getId();
                return $this->redirect($this->generateUrl('app_mostrarNoticia',$params));
            }
        }
        return $this->render('AppBundle:Default:formAfegirNoticia.html.twig', array('form'=>$form->createView()));
    }

    //MOSTRAR UNA NOTICIA
    public function mostrarNoticiaAction($id){
        $em =$this->getDoctrine()->getManager();
        $noticia=$em->getRepository('AppBundle:noticia')->find($id);
        if (!$noticia) {
            $mensaje=array('error'=>'La noticia seleccionada no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $params=array(
            'noticia'=>$noticia,
        );

        return $this->render('AppBundle:Default:mostrarNoticia.html.twig', $params);
    }

    //ELIMINAR NOTICIA
    public function eliminarNoticiaAction($id){
        $em =$this->getDoctrine()->getManager();
        $noticia= $em->getRepository('AppBundle:noticia')->find($id);
        if(!$noticia){
            $mensaje=array('error'=>'La noticia seleccionada no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        //controla que l'usuari que ha creat la noticia coincideixca amb l'usuari logejat o siga moderador
        if(!$this->isGranted('ROLE_MODERADOR')){
            if($this->getUser()->getUsername()!= $noticia->getUsuarios()->getUsername()){
                $mensaje=array('error'=>'No tens permís per eliminar aquesta noticia.');
                return($this->render('AppBundle:Default:error.html.twig',$mensaje));
            }
        }

        $em->remove($noticia);
        $em->flush();
        return $this->redirect($this->generateUrl('app_llistarNoticies'));
    }

    //LLISTAR TOTES LES NOTICIES
    public function llistarNoticiesAction(Request $request){
        $em = $this->getDoctrine()->getRepository('AppBundle:noticia');
        $qb = $em->createQueryBuilder('noticia');
        $qb->orderBy('noticia.fecha', 'desc');
        $query = $qb->getQuery();

        $noticiesPaginades = $this->paginacio($request, $query, 5);

        $params=array(
            'noticies'=>$noticiesPaginades
        );

        return $this->render('AppBundle:Default:llistarNoticies.html.twig', $params);
    }
}
<?php

namespace AppBundle\Controller;


use AppBundle\Entity\asiste;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Form\Type\usuariosType;
use AppBundle\Form\Type\obraType;
use AppBundle\Form\Type\partituraType;
use AppBundle\Form\Type\agrupacionType;
use AppBundle\Form\Type\eventoType;
use AppBundle\Form\Type\tocaType;
use AppBundle\Form\Type\comentariosType;
use AppBundle\Entity\usuarios;
use AppBundle\Entity\obra;
use AppBundle\Entity\partitura;
use AppBundle\Entity\evento;
use AppBundle\Entity\agrupacion;
use AppBundle\Entity\toca;

use AppBundle\Entity\comentarios;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Default:index.html.twig');
    }

//----------------------------ZONA D'USUARIS---------------------------------
    //REGISTRE
    public function registreAction(Request $request){
        if ($this->getUser()){
            $params['id']=$this->getUser()->getId();
            return $this->redirect($this->generateUrl('app_modificarUsuari',$params));
        }
        $em =$this->getDoctrine()->getManager();

        $usuari=new usuarios();
        $usuari->setRol('ROLE_USUARI');
        $usuari->setBaixa('No');


        //CREA EL FORMULARI
        $form= $this->createForm(new usuariosType(), $usuari, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST'){
            $form->bind($request);

            if($form->isValid()){
                //Imatge perfil
                $dir='bundles/AppBundle/banda/perfils/';
                if($form['imagenPerfil']->getData()){
                    $archivo=$form['imagenPerfil']->getData();
                    $nombre=time().$form['imagenPerfil']->getData()->getClientOriginalName();
                    $imageNova=$this->redimensionaImatge($archivo);

                    //imagejpg() copia la image al directori
                    imagejpeg($imageNova, $dir.$nombre);
                    $usuari->setImagenPerfil($nombre);
                }
                else{
                    if($usuari->getImagenPerfil()==NULL){
                        $imagen='default.png';
                        $usuari->setImagenPerfil($imagen);
                    }
                }

                $factory=$this->get('security.encoder_factory');
                $encoder=$factory->getEncoder($usuari);
                $password=$encoder->encodePassword($usuari->getPassword(), $usuari->getSalt());
                $usuari->setPassword($password);
                //Comprobació per vorer si algún usuari ja té el username indicat.
                $userExist = $em->getRepository('AppBundle:usuarios')->findByUsername($form['username']->getData());
                if ($userExist) {
                    $mensaje=array('error'=>'Ja existeix un usuari amb eixe nom d\'usuari.');
                    return($this->render('AppBundle:Default:error.html.twig',$mensaje));
                }

                $em->persist($usuari);
                $em->flush();
                return $this->redirect($this->generateUrl('app_homepage'));
            }
        }
        return $this->render('AppBundle:Default:formRegistre.html.twig', array('form'=>$form->createView()));
    }

    //MODIFICAR USUARI
    public function modificarUsuariAction(Request $request, $id){
        $em =$this->getDoctrine()->getManager();
        $usuari=$em->getRepository('AppBundle:usuarios')->find($id);

        //controla que l'usuari coincidisca amb l'usuari logejat o siga admin
        if(!$this->isGranted('ROLE_ADMIN')){
            if($this->getUser()->getUsername()!= $usuari->getUsername()){
                $mensaje=array('error'=>'No tens permís per a editar aquest perfil.');
                return($this->render('AppBundle:Default:error.html.twig',$mensaje));
            }
        }
        $pass=$usuari->getPassword();
        $nick=$usuari->getUsername();
        $img=$usuari->getImagenPerfil();
        //crea el formulari
        $form= $this->createForm(new usuariosType(), $usuari, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST'){
            $form->bind($request);

            if($form->isValid()){
                //Imatge perfil
                $dir='bundles/AppBundle/banda/perfils/';

                if ($form['imagenPerfil']->getData()) {
                    $archivo = $form['imagenPerfil']->getData();
                    $nombre=time().$form['imagenPerfil']->getData()->getClientOriginalName();
                    $imageNova=$this->redimensionaImatge($archivo);

                    //imagejpg() copia la image al directori
                    imagejpeg($imageNova, $dir.$nombre);
                    $usuari->setImagenPerfil($nombre);
                } else {
                    if ($usuari->getImagenPerfil() !== 'default.png'){
                        $usuari->setImagenPerfil($img);
                    }
                }
                //comproba si el check está marcar per donarlo de baixa
                if($form['isBaixa']->getData()==true){
                    $usuari->setBaixa('Sí');
                } else {
                    $usuari->setBaixa('No');
                }

                $usuari->setPassword($pass);
                $usuari->setUserName($nick);
                $em->persist($usuari);
                $em->flush();
                $params['id']=$usuari->getId();
                return $this->redirect($this->generateUrl('app_perfil',$params));
            }
        }
        return $this->render('AppBundle:Default:formModificarUsuari.html.twig', array('form'=>$form->createView()));
    }

    //MODIFICAR PASSWORD
    public function modificarPasswordAction(Request $request, $id){
        $em =$this->getDoctrine()->getManager();
        $usuari=$em->getRepository('AppBundle:usuarios')->find($id);

        //controla que l'usuari coincidisca amb l'usuari logejat
        if($id && $this->getUser()->getUsername()!= $usuari->getUsername() && $this->getUser()->getRol()!='ROLE_ADMIN'){
            $mensaje=array('error'=>'No tens permís per a editar aquest perfil.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }
        $nick=$usuari->getUsername();
        $img=$usuari->getImagenPerfil();
        $nom=$usuari->getNombre();
        $ape=$usuari->getApellidos();
        $tel=$usuari->getTelefono();
        $actiu=$usuari->getBaixa();
        //crea el formulari
        $form= $this->createForm(new usuariosType(), $usuari, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST'){
            $form->bind($request);

            if($form->isValid()){
                $factory=$this->get('security.encoder_factory');
                $encoder=$factory->getEncoder($usuari);
                $password=$encoder->encodePassword($usuari->getPassword(), $usuari->getSalt());
                $usuari->setPassword($password);
                $usuari->setTelefono($tel);
                $usuari->setApellidos($ape);
                $usuari->setImagenPerfil($img);
                $usuari->setNombre($nom);
                $usuari->setUserName($nick);
                $usuari->setBaixa($actiu);
                $em->persist($usuari);
                $em->flush();
                $params['id']=$usuari->getId();
                return $this->redirect($this->generateUrl('app_perfil',$params));
            }
        }
        return $this->render('AppBundle:Default:formModificarPassword.html.twig', array('form'=>$form->createView()));
    }

    //PERFIL D'USUARI
    public function perfilAction($id){
        $em =$this->getDoctrine()->getManager();
        $usuari=$em->getRepository('AppBundle:usuarios')->find($id);
        $toca=$em->getRepository('AppBundle:toca')->findByIdUsuario($id);
        $params=array(
            'usuari'=>$usuari,
            'toca'=>$toca
        );

        return $this->render('AppBundle:Default:perfil.html.twig', $params);
    }

    //ASSIGNAR PERMISOS
    public function gestioUsuarisAction(Request $request){
        $em = $this->getDoctrine()->getRepository('AppBundle:usuarios');
        $query = $em->createQueryBuilder('usuarios');

        if(isset($_GET['music'])) {
            $query->andWhere('usuarios.nombre LIKE :nom');
            $query->orWhere('usuarios.apellidos LIKE :ape');
            $query->setParameter('nom', '%' . $_GET['music'] . '%');
            $query->setParameter('ape', '%' . $_GET['music'] . '%');
        }
        $query->orderBy('usuarios.nombre, usuarios.apellidos', 'asc');
        $usuaris = $this->paginacio($request, $query, 10);

        $params=array(
            'usuaris'=>$usuaris
        );
        return($this->render('AppBundle:Default:gestioUsuaris.html.twig',$params));
    }

    public function permisosAction(Request $request, $id){
        $em =$this->getDoctrine()->getManager();
        //Comproba que l'usuari existeix
        $usuari=$em->getRepository('AppBundle:usuarios')->find($id);
        if (!$usuari) {
            $mensaje=array('error'=>'L\'usuari seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }
        $nom=$usuari->getNombre();
        $ape=$usuari->getApellidos();
        $usr=$usuari->getUsername();
        $pass=$usuari->getPassword();
        $tel=$usuari->getTelefono();
        $img=$usuari->getImagenPerfil();
        $actiu=$usuari->getBaixa();

        $form= $this->createForm(new usuariosType($em, $id), $usuari, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST') {
            $form->bind($request);
            $rol="ROLE_USUARI,";
            if($form['music']->getData()==true){
                $rol.='ROLE_MUSIC,';
            }
            if($form['arxiver']->getData()==true){
                $rol.='ROLE_DIRECTIU,ROLE_ARXIVER,';
            }
            if($form['administrador']->getData()==true){
                $rol.='ROLE_DIRECTIU,ROLE_ADMIN,';
            }
            if($form['directiu']->getData()==true){
                $rol.='ROLE_DIRECTIU,';
            }
            if($form['president']->getData()==true){
                $rol.='ROLE_DIRECTIU,ROLE_PRESIDENT,';
            }
            if($form['magatzem']->getData()==true){
                $rol.='ROLE_DIRECTIU,ROLE_MAGATZEM,';
            }
            if($form['secretari']->getData()==true){
                $rol.='ROLE_DIRECTIU,ROLE_SECRETARI,';
            }
            if($form['tresorer']->getData()==true){
                $rol.='ROLE_DIRECTIU,ROLE_TRESORER,';
            }
            if($form['moderador']->getData()==true){
                $rol.='ROLE_DIRECTIU,ROLE_MODERADOR,';
            }

            if($form->isValid()){
                $usuari->setNombre($nom);
                $usuari->setApellidos($ape);
                $usuari->setUsername($usr);
                $usuari->setPassword($pass);
                $usuari->setTelefono($tel);
                $usuari->setImagenPerfil($img);
                $usuari->setRol(rtrim($rol,","));
                $usuari->setBaixa($actiu);
                $em->persist($usuari);
                $em->flush();

                $params['id']=$usuari->getId();
                return $this->redirect($this->generateUrl('app_perfil',$params));
            }
        }
        return $this->render('AppBundle:Default:formPermisos.html.twig', array('form'=>$form->createView()));
    }


//----------------------------ZONA D'ADMINISTRACIÓ DE MÚSICS--------------------------------
    //AFEGIR AGRUPACIO (ADMINISTRACIÓ D'ENTITAT)
    public function afegirAgrupacioAction(Request $request, $id){
        $em =$this->getDoctrine()->getManager();
        if(is_null($id)){
            $agrupacio=new agrupacion();
        }
        else{
            $agrupacio=$em->getRepository('AppBundle:agrupacion')->find($id);
        }

        $form= $this->createForm(new agrupacionType(), $agrupacio, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST') {

            $form->bind($request);
            if($form->isValid()){
                $em->persist($agrupacio);
                $em->flush();
                return $this->redirect($this->generateUrl('app_homepage'));
            }
        }
        return $this->render('AppBundle:Default:formAfegirAgrupacio.html.twig', array('obra'=>$agrupacio, 'form'=>$form->createView()));
    }

    //AFEGIR COMPETENCIES, MOSTRA ELS USUARIS I ENLLAÇARÀ A ASIGNARCOMPETENCIA
    public function afegirCompetenciaAction(Request $request){
        $em = $this->getDoctrine()->getRepository('AppBundle:usuarios');
        $usuaris= $em->findAll();
        $idsMusics = array();
        foreach($usuaris as $users){
            if (in_array('ROLE_MUSIC' , $users->getRoles())){
                array_push($idsMusics, $users);
            }
        }
        $capDeCorda= $this->getDoctrine()->getRepository('AppBundle:toca')->findBy(array('idUsuario' => $this->getUser()->getId(),'capDeCorda' => 'Sí'));
        //Comproba si eres director en alguna agrupació o cap de corda
        if (!$capDeCorda and !$this->comprobarDirector() and !$this->isGranted('ROLE_PRESIDENT')){
            $mensaje=array('error'=>'No ets director.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $query = $em->createQueryBuilder('usuarios');
        if(isset($_GET['music'])) {
            $query->andWhere('usuarios.nombre LIKE :nom');
            $query->orWhere('usuarios.apellidos LIKE :ape');
            $query->setParameter('nom', '%' . $_GET['music'] . '%');
            $query->setParameter('ape', '%' . $_GET['music'] . '%');
        }
        $query->andWhere('usuarios.id IN (:ids_users)');
        $query->setParameter('ids_users', $idsMusics);
        $query->orderBy('usuarios.nombre, usuarios.apellidos', 'asc');

        $musics = $this->paginacio($request, $query, 10);
        $params=array(
            'usuari'=>$musics
        );
        return $this->render('AppBundle:Default:afegirCompetencia.html.twig', $params);
    }

    //ASSIGNAR INSTRUMENT I AGRUPACIO
    public function asignarCompetenciaAction(Request $request, $id){
        $em =$this->getDoctrine()->getManager();
        //Comproba que l'usuari existeix
        $usuari=$em->getRepository('AppBundle:usuarios')->find($id);
        if (!$usuari) {
            $mensaje=array('error'=>'L\'usuari seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }
        $capDeCorda=$em->getRepository('AppBundle:toca')->findBy(array('idUsuario' => $this->getUser()->getId(),'capDeCorda' => 'Sí'));

        if (!$capDeCorda and !$this->comprobarDirector() and !$this->isGranted('ROLE_PRESIDENT')){
            $mensaje=array('error'=>'No ets director.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $toca=new toca();
        //Esta comprobació será l'encarregada de montar el disseny del combobox i sel passarà ja montat al Form
        if ($this->comprobarDirector()){
            $agrupacions = $this->comprobarAgrupacionsDelDirector();

            $agrupacionsComboBox = array();
            foreach($agrupacions as $agrup){
                $agrupacionsComboBox[$agrup->getIdAgrupacion()->getId()]=$agrup->getIdAgrupacion()->getNombre();
            }
        } elseif ($this->isGranted('ROLE_PRESIDENT')){
            $agrupacions = $em->getRepository('AppBundle:agrupacion')->findAll();

            $agrupacionsComboBox = array();
            foreach($agrupacions as $agrup){
                $agrupacionsComboBox[$agrup->getId()]=$agrup->getNombre();
            }
        } else {
            $agrupacionsComboBox= array();
            foreach($capDeCorda as $cdp){
                $agrupacionsComboBox[$cdp->getIdAgrupacion()->getId()]=$cdp->getIdAgrupacion()->getNombre();
            }
        }

        $form= $this->createForm(new tocaType($em, $id, $agrupacionsComboBox), $toca, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST') {
            $form->bind($request);

            $usu=$form['id_usuario']->getData();
            $inst=$form['id_instrumento']->getData();
            $agr=$form['id_agrupacion']->getData();
            $agrupacio=$em->getRepository('AppBundle:agrupacion')->find($agr);
            $usuari=$em->getRepository('AppBundle:usuarios')->find($usu);
            $instrument=$em->getRepository('AppBundle:instrumento')->find($inst);

            //Fará una consulta en toca per vorer si l'usuari ja toca a l'agrupació seleccionada
            $comprobacio=$em->getRepository('AppBundle:toca')->findByIdUsuario($id);
            foreach ($comprobacio as $result){
                if($result->getIdAgrupacion() === $agrupacio){
                    $mensaje=array('error'=>'Aquest músic ja toca un instrument a aquesta agrupació.');
                    return($this->render('AppBundle:Default:error.html.twig',$mensaje));
                }
            }

            if($form->isValid()){
                $toca->setIdUsuario($usuari);
                $toca->setIdAgrupacion($agrupacio);
                $toca->setIdInstrumento($instrument);
                if($form['capDeCorda']->getData()==true){
                    $toca->setCapDeCorda('Sí');
                } else {
                    $toca->setCapDeCorda('No');
                }

                if($toca->getIdInstrumento()->getNombre() == 'Director'){$toca->setCapDeCorda('Sí');}
                $em->persist($toca);
                $em->flush();

                $params['id']=$usuari->getId();
                return $this->redirect($this->generateUrl('app_perfil',$params));
            }
        }
        return $this->render('AppBundle:Default:formAsignarCompetencia.html.twig', array('form'=>$form->createView()));
    }

    //ADMINISTRAR COMPETENCIES, LA RELACIO ENTRE MUSIC AGRUPACIO I INSTRUMENT DESDE MUSICS
    public function administrarCompetenciaAction(Request $request){
        $em =$this->getDoctrine()->getManager();

        //Cree els arrays en els id de les coincidencies trobades per POST
        $idMusics = array();
        $idAgrupacions = array();
        $idInstruments = array();
        if(isset($_GET['agrupacio'])||isset($_GET['instrument'])||isset($_GET['music'])) {
            if ($_GET['agrupacio'] != "") {
                $filtreAgrupacio = "SELECT agrupacion FROM AppBundle:agrupacion agrupacion";
                $filtreAgrupacio .= " WHERE agrupacion.nombre LIKE :agrup";
                $query = $em->createQuery($filtreAgrupacio);
                $query->setParameter('agrup', '%'.$_GET['agrupacio'].'%');

                foreach ($query->getResult() as $agrupacioFiltrat){
                    array_push($idAgrupacions, $agrupacioFiltrat->getId());
                }
            }
            if ($_GET['instrument'] != "") {
                $filtreInstrument = "SELECT instrumento FROM AppBundle:instrumento instrumento";
                $filtreInstrument .= " WHERE instrumento.nombre LIKE :instr";
                $query = $em->createQuery($filtreInstrument);
                $query->setParameter('instr', '%'.$_GET['instrument'].'%');

                foreach ($query->getResult() as $instrumentFiltrat){
                    array_push($idInstruments, $instrumentFiltrat->getId());
                }
            }
            if ($_GET['music'] != "") {
                $filtreMusic = "SELECT usuarios FROM AppBundle:usuarios usuarios";
                $filtreMusic .= " WHERE usuarios.nombre LIKE :music";
                $query = $em->createQuery($filtreMusic);
                $query->setParameter('music', '%'.$_GET['music'].'%');

                foreach ($query->getResult() as $musicFiltrat){
                    array_push($idMusics, $musicFiltrat->getId());
                }
            }
        }
        //Consulta en queryBuilder
        $compe = $this->getDoctrine()->getRepository('AppBundle:toca');
        $qb = $compe->createQueryBuilder('toca');
        //El knp paginator no acepta orderBy un número, per tant hem de agrupar per id.
        $qb->groupBy('toca.idAgrupacion, toca.idInstrumento, toca.idUsuario');
        //ANTERIOR ----->    $qb->orderBy('toca.idAgrupacion, toca.idInstrumento, toca.idUsuario');
        if(isset($_GET['agrupacio'])||isset($_GET['instrument'])||isset($_GET['music'])) {
            if($_GET['agrupacio']!=""){
                $qb->andWhere('toca.idAgrupacion IN (:ids_agrup)');
                if($_GET['instrument']!=""){
                    dump('test');
                    $qb->andWhere('toca.idInstrumento IN (:ids_instr)');
                    if($_GET['music']!=""){
                        $qb->andWhere('toca.idUsuario IN (:ids_usua)')
                            ->setParameter('ids_agrup', $idAgrupacions)
                            ->setParameter('ids_instr', $idInstruments)
                            ->setParameter('ids_usua', $idMusics);
                    }
                    $qb->setParameter('ids_agrup', $idAgrupacions)
                        ->setParameter('ids_instr', $idInstruments);
                }
                else if($_GET['music']!=""){
                    $qb->andWhere('toca.idUsuario IN (:ids_usua)')
                        ->setParameter('ids_agrup', $idAgrupacions)
                        ->setParameter('ids_usua', $idMusics);
                }
                $qb->setParameter('ids_agrup', $idAgrupacions);
            }
            else if($_GET['instrument']!=""){

                $qb->andWhere('toca.idInstrumento IN (:ids_instr)');
                if($_GET['music']!=""){
                    $qb->andWhere('toca.idUsuario IN (:ids_usua)')
                        ->setParameter('ids_instr', $idInstruments)
                        ->setParameter('ids_usua', $idMusics);
                }
                $qb->setParameter('ids_instr', $idInstruments);
            }
            else if($_GET['music']!=""){
                $qb->andWhere('toca.idUsuario IN (:ids_usua)')
                    ->setParameter('ids_usua', $idMusics);
            }
        }
        $query = $qb->getQuery();
        $competencies = $this->paginacio($request, $query, 20);

        $agrs = array();
        foreach((array)$this->comprobarAgrupacionsDelDirector() as $objToca) {
            array_push($agrs, $objToca->getIdAgrupacion()->getNombre());
        }

        $capDeCorda=$em->getRepository('AppBundle:toca')->findBy(array('idUsuario' => $this->getUser()->getId(),'capDeCorda' => 'Sí'));

        $params=array(
            'competencies'=>$competencies,
            'director' =>$this->comprobarDirector(),
            'agrups'=>$agrs,
            'capCorda' => $capDeCorda
        );
        return $this->render('AppBundle:Default:administrarCompetencia.html.twig', $params);
    }

    //ELIMINAR COMPETENCIA, RELACIO ENTRE USUARI, AGRUPACIO I INSTRUMENT
    public function eliminarCompetenciaAction($id){
        $em =$this->getDoctrine()->getManager();

        //Comproba que la competencia pasada per id existeixca
        $competencia=$em->getRepository('AppBundle:toca')->find($id);
        if (!$competencia) {
            $mensaje=array('error'=>'No s\'han trobat coincidencies.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $capDeCorda=$em->getRepository('AppBundle:toca')->findBy(array('idUsuario' => $this->getUser()->getId(),'capDeCorda' => 'Sí'));

        //Comproba si no eres director
        if (!$capDeCorda and !$this->comprobarDirector() and !$this->isGranted('ROLE_ADMIN')) {
            $mensaje = array('error' => 'No ets director.');
            return ($this->render('AppBundle:Default:error.html.twig', $mensaje));
        }//Comproba si eres director en alguna agrupació
        if ($this->comprobarDirector()){
            //Si eres director comproba que l'agrupació de la que eres director y la agrupació que fa el concert siga igual
            $event=$em->getRepository('AppBundle:toca')->find($id);
            $agrs = array();
            foreach((array)$this->comprobarAgrupacionsDelDirector() as $objToca) {
                array_push($agrs, $objToca->getIdAgrupacion()->getNombre());
            }

            if (!in_array($event->getIdAgrupacion()->getNombre(), $agrs)){
                $mensaje=array('error'=>'No ets director d\'aquesta agrupació');
                return($this->render('AppBundle:Default:error.html.twig',$mensaje));
            }
        }
        //Comproba si eres Cap de corda
        if($capDeCorda){
            foreach ($capDeCorda as $result){
                //Si el instrument del 'toca' que vols borrar no coincideix en el instrument que eres capDeCorda
                //y la agrupació de 'toca' es la mateixa, saltarà l'error.
                if (($competencia->getIdInstrumento() != $result->getIdInstrumento())
                    and ($competencia->getIdAgrupacion() == $result->getIdAgrupacion())){
                    $mensaje=array('error'=>'No ets ni director, ni cap de corda d\'aquest instrument.');
                    return($this->render('AppBundle:Default:error.html.twig',$mensaje));
                }
            }

        }

        //Elimina la competencia
        $em->remove($competencia);
        $em->flush();

        return $this->redirect($this->generateUrl('app_administrarCompetencia'));
    }


//-----------------------------ZONA ARXIU--------------------------------
    //AFEGIR OBRA
    public function afegirObraAction(Request $request, $id){
        //Aquesta comprobació comprobará que si no eres director o arxiver no es deixe accedir a la vista
        if (!$this->comprobarDirector() and !$this->isGranted('ROLE_ARXIVER')){
            $mensaje=array('error'=>'Només arxivers o directors podrán afegir un obra.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $em =$this->getDoctrine()->getManager();
        if(is_null($id)){
            $obra=new obra();
            $descripcio=null;
            $video=null;
        }
        else{
            $obra=$em->getRepository('AppBundle:obra')->find($id);
            $nomOriginal=$obra->getNombre();
            $descripcio=$obra->getDescripcion();
            $video=$obra->getVideo();
        }
        $form= $this->createForm(new obraType(), $obra, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST') {
            $form->bind($request);
            if($form->isValid()){
                $nom=$form['nombre']->getData();
                ///CODI NOU!!!!!!!!!!!!!!!
                $obra->setDescripcion($descripcio);
                $obra->setVideo($video);
                if($this->comprobarDirector() or $this->isGranted('ROLE_ADMIN')){
                    $obra->setDescripcion($form['descripcion']->getData());
                    $url=$form['video']->getData();
                    /// EXEMPLE, donada la URL. https://www.youtube.com/watch?v=IzAcW7dqAQo&t=1231s
                    //El primer explode agafará IzAcW7dqAQo&t=1231s y el segon 'IzAcW7dqAQo', este codi será el que es posse al iframe
                    $codiFinal=$url;
                    if (strpos($url, '?v=')){
                        $urlPartida = explode('?v=', $url);
                        $codiFinal = $urlPartida[1];
                        if (strpos($urlPartida[1], '&')){
                            $codiFinal = explode('&',$urlPartida[1]);
                            $codiFinal = $codiFinal[0];
                        }
                    }
                    $obra->setVideo($codiFinal);
                }
                //Si està creantse una nova obra (si es modificar no torna a crear el dir)
                if(is_null($id)){
                    mkdir('bundles/AppBundle/Obres/'.$nom);

                }
                else{
                    if ($handle = opendir('bundles/AppBundle/Obres/'.$nomOriginal)) {
                        while (false !== ($file = readdir($handle))) {
                            if (is_file('bundles/AppBundle/Obres/'.$nomOriginal.'/'.$file)) {
                                $oldname   = 'bundles/AppBundle/Obres/'.$nomOriginal.'/'.$file;
                                $nou= strstr($file,"-");

                                rename($oldname, 'bundles/AppBundle/Obres/'.$nomOriginal.'/'.$obra->getNombre().' '.$nou);
                            }
                        }
                        closedir($handle);
                    }
                    rename('bundles/AppBundle/Obres/'.$nomOriginal,'bundles/AppBundle/Obres/'.$nom);

                }

                $em->persist($obra);
                $em->flush();
                return $this->redirect($this->generateUrl('app_llistarObres'));
            }
        }
        return $this->render('AppBundle:Default:formAfegirObra.html.twig', array('form'=>$form->createView(), 'director'=>$this->comprobarDirector()));
    }

    //MOSTRAR UNA OBRA
    public function mostrarObraAction($id){
        $em =$this->getDoctrine()->getManager();
        $obra=$em->getRepository('AppBundle:obra')->find($id);
        if (!$obra) {
            $mensaje=array('error'=>'L\'obra seleccionada no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $director = $this->comprobarDirector();
        //En el cas que sigues Director d'alguna agrupació o arxiver mostrará totes les partitures de l'obra ordenades per instrument
        if ($director or $this->isGranted('ROLE_ARXIVER')){
            $dql = "SELECT partitura FROM AppBundle:partitura partitura";
            $dql .= " where partitura.obra = :id order by partitura.instrumento";
            $query = $em->createQuery($dql);
            $query->setParameter('id', $id);
            $partitures = $query->getResult();

            //En cas que sigues un músic només es mostrará les partitures dels instruments que toques a les diferents agrupacions
        } else {
            $toca=$em->getRepository('AppBundle:toca')->findByIdUsuario($this->getUser()->getId());

            //Instrodueix al array tots els intruments que tocará el músic a les diferents agrupacions de l'entitat.
            $instruments = array();
            foreach ($toca as $result){
                array_push($instruments, $result->getIdInstrumento()->getNombre());
            }

            $consulta = $this->getDoctrine()->getRepository('AppBundle:partitura');
            $qb = $consulta->createQueryBuilder('partitura');
            $qb->andWhere('partitura.obra = :idObra')
                ->andWhere('partitura.instrumento IN (:noms_instr)')
                ->setParameter('idObra', $id)
                ->setParameter('noms_instr', $instruments)
                ->orderBy('partitura.instrumento', 'asc');
            $query = $qb->getQuery();
            $partitures = $query->getResult();
        }

        $params=array(
            'partitures'=>$partitures,
            'obra'=>$obra,
            'director' => $director
        );

        return $this->render('AppBundle:Default:mostrarObra.html.twig', $params);
    }

    //MOSTRAR LLISTAT D'OBRES
    public function llistarObresAction(Request $request){

        $em =$this->getDoctrine()->getManager();
        $totes= $em->getRepository('AppBundle:obra')->findAll();
        //Genera la consuta amb totes les obres
        $dql = "SELECT obra FROM AppBundle:obra obra";

        //Comprova si s'ha pulsat el botó per a filtrar
        if(isset($_GET['genere'])||isset($_GET['nombre'])||isset($_GET['compositor'])) {
            if($_GET['genere']!=""){
                $dql.=" WHERE obra.genero LIKE :gen";
                if($_GET['nombre']!=""){
                    $dql.=" AND obra.nombre LIKE :nom";
                    if($_GET['compositor']!=""){
                        $dql.=" AND obra.compositor LIKE :comp";
                    }
                }
                else if($_GET['compositor']!=""){
                    $dql.=" AND obra.compositor LIKE :comp";
                }
            }
            else if($_GET['nombre']!=""){
                $dql.=" WHERE obra.nombre LIKE :nom";
                if($_GET['compositor']!=""){
                    $dql.=" AND obra.compositor LIKE :comp";
                }
            }

            else if($_GET['compositor']!=""){
                $dql.=" WHERE obra.compositor LIKE :comp";
            }
        }

        //Crea la consulta
        $query = $em->createQuery($dql);

        //Assigna els paràmetres (comprobant si s'ha pulsat el botó i si cada camp te valors)
        if(isset($_GET['genere'])||isset($_GET['nombre'])||isset($_GET['compositor'])) {
            if ($_GET['nombre'] != "") {
                $query->setParameter('nom', '%'.$_GET['nombre'].'%');
            }
            if ($_GET['genere'] != "") {
                $query->setParameter('gen', $_GET['genere']);
            }
            if ($_GET['compositor'] != "") {
                $query->setParameter('comp', '%'.$_GET['compositor'].'%');
            }
        }

        $genere= array();
        foreach($totes as $result){
            $gen=$result->getGenero();
            if(!in_array($gen, $genere)){
                array_push($genere, $gen);
            }
        }

        $arxiuPaginat = $this->paginacio($request, $query, 10);

        $params=array(
            'paginacio'=>$arxiuPaginat,
            'genere'=>$genere,
            'director' =>$this->comprobarDirector()
        );
        return $this->render('AppBundle:Default:llistarobres.html.twig',$params);
    }

    //ELIMINAR OBRA
    public function eliminarObraAction($id){
        $em =$this->getDoctrine()->getManager();
        $obra= $em->getRepository('AppBundle:obra')->find($id);
        //rmdir($obra->getNombre());
        $em->remove($obra);
        $em->flush();
        return $this->redirect($this->generateUrl('app_llistarObres'));
    }

    //AFEGIR PARTITURA
    public function afegirPartituraAction(Request $request, $id){
        $em =$this->getDoctrine()->getManager();
        $obra= $em->getRepository('AppBundle:obra')->find($id);

        if (!$this->comprobarDirector() and !$this->isGranted('ROLE_ARXIVER')){
            $mensaje=array('error'=>'No ets director.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $partitura=new partitura();
        $form= $this->createForm(new partituraType($em), $partitura, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST') {

            $form->bind($request);
            if($form->isValid()){
                $partitura->setObra($obra);
                $partitura->setInstrumento($form['instrumento']->getData());
                $partitura->setVoz($form['veu']->getData());
                $nom=$obra->getNombre().' - '.$partitura->getInstrumento().' '.$partitura->getVoz().'.pdf';
                $dir='bundles/AppBundle/Obres/'.$obra->getNombre();
                $partitura->setPdf($nom);
                $form['pdf']->getData()->move($dir,$nom);
                $em->persist($partitura);
                $em->flush();

                $params['id']=$obra->getId();
                return $this->redirect($this->generateUrl('app_mostrarObra',$params));
            }
        }
        return $this->render('AppBundle:Default:formAfegirPartitura.html.twig', array('obra'=>$obra, 'form'=>$form->createView()));
    }


//-------------------------------ZONA ESDEVENIMENTS-----------------------------
    //AFEGIR UN EVENT
    public function afegirEventAction(Request $request, $id){
        //Aquesta comprobació comprobará que si eres director
        if (!$this->comprobarDirector() and !$this->isGranted('ROLE_ADMIN')){
            $mensaje=array('error'=>'No ets director.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }
        $em =$this->getDoctrine()->getManager();
        if(is_null($id)){
            $event=new evento();
        }
        else{
            $event=$em->getRepository('AppBundle:evento')->find($id);
            //Si eres músic pero no perteneixes a la agrupació seleccionada
            $toca=$em->getRepository('AppBundle:toca')->findBy(array('idUsuario'=>$this->getUser()->getId(), 'idAgrupacion'=>$event->getAgrupacion()->getId()));
            if (!$toca && !$this->isGranted('ROLE_ADMIN')) {
                $mensaje=array('error'=>'No pots modificar aquest esdeveniment ja que no eres el director d\'aquesta agrupació.');
                return($this->render('AppBundle:Default:error.html.twig',$mensaje));
            }
        }

        //Esta comprobació será l'encarregada de montar el disseny del combobox i sel passarà ja montat al Form
        if ($this->comprobarDirector()){
            $agrupacions = $this->comprobarAgrupacionsDelDirector();

            $agrupacionsComboBox = array();
            foreach($agrupacions as $agrup){
                $agrupacionsComboBox[$agrup->getIdAgrupacion()->getId()]=$agrup->getIdAgrupacion()->getNombre();
            }
        } else {
            //Codi Antic que estaba al ConciertoType
            $agrupacions = $em->getRepository('AppBundle:agrupacion')->findAll();

            $agrupacionsComboBox = array();
            foreach($agrupacions as $agrup){
                $agrupacionsComboBox[$agrup->getId()]=$agrup->getNombre();
            }
        }

        $form= $this->createForm(new eventoType($agrupacionsComboBox), $event, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST') {

            $form->bind($request);
            if($form->isValid()){
                //agafa l'agrupació buscada pel id del formulari (perque s'ha de  passar l'objecte i no sols el ID)
                $agrupacio=$em->getRepository('AppBundle:agrupacion')->find($form['agrupacion']->getData());
                $event->setAgrupacion($agrupacio);
                $event->setTipo($form['tipo']->getData());
                $em->persist($event);
                $em->flush();
                $params['id']=$event->getId();

                //Assistencia
                if(is_null($id)){
                    $usuaris=$em->getRepository('AppBundle:usuarios')->findAll();
                    foreach($usuaris as $result){
                        $asist=new asiste();
                        $asist->setAsiste('Per confirmar');
                        $asist->setUsuarioId($result);
                        $asist->setEventoId($event);

                        $em->persist($asist);
                    }
                    $em->flush();
                }
                return $this->redirect($this->generateUrl('app_mostrarEvent',$params));
            }
        }
        return $this->render('AppBundle:Default:formAfegirEvent.html.twig', array('obra'=>$event, 'form'=>$form->createView()));
    }

    //MOSTRA UN EVENT
    public function mostrarEventAction($id){
        $em =$this->getDoctrine()->getManager();
        $concert=$em->getRepository('AppBundle:evento')->find($id);

        if (!$concert) {
            $mensaje=array('error'=>'El event seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $toca=$em->getRepository('AppBundle:toca')->findBy(array('idUsuario'=>$this->getUser()->getId(), 'idAgrupacion'=>$concert->getAgrupacion()->getId()));
        $agrs = array();
        foreach((array)$this->comprobarAgrupacionsDelDirector() as $objToca) {
            array_push($agrs, $objToca->getIdAgrupacion()->getNombre());
        }

        if($toca){$exist = true;}else{$exist = false;}

        $params=array(
            'event'=>$concert,
            'director' => $this->comprobarDirector(),
            'agrupacioDirector' => $agrs,
            'exist' => $exist
        );
        return $this->render('AppBundle:Default:mostrarEvent.html.twig', $params);
    }

    //ASSIGNAR OBRA A EVENT VISTA
    public function asignarObraAEventAction($idEvent, Request $request){
        $em =$this->getDoctrine()->getManager();
        $concert=$em->getRepository('AppBundle:evento')->find($idEvent);
        if (!$concert) {
            $mensaje=array('error'=>'El event seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        //Comproba si no eres director
        if (!$this->comprobarDirector() and !$this->isGranted('ROLE_ADMIN')) {
            $mensaje = array('error' => 'No ets director.');
            return ($this->render('AppBundle:Default:error.html.twig', $mensaje));
        }//Comproba si eres director en alguna agrupació
        if ($this->comprobarDirector()){
            //Si eres director comproba que l'agrupació de la que eres director y la agrupació que fa el event siga igual
            $agrs = array();
            foreach((array)$this->comprobarAgrupacionsDelDirector() as $objToca) {
                array_push($agrs, $objToca->getIdAgrupacion()->getNombre());
            }
            if (!in_array($concert->getAgrupacion()->getNombre(), $agrs)){
                $mensaje=array('error'=>'No ets director d\'aquesta agrupació.');
                return($this->render('AppBundle:Default:error.html.twig',$mensaje));
            }
        }

        //Es fa un array en les obres que están en programa
        $idsObresEnPrograma=Array();
        foreach ($concert->getObra() as $result){
            array_push($idsObresEnPrograma, $result->getId());
        }
        //Es crea la consulta sense les obres que ja están a programa
        $em = $this->getDoctrine()->getRepository('AppBundle:obra');
        $qb = $em->createQueryBuilder('obra');
        dump($concert->getObra());
        if (!empty($idsObresEnPrograma)){
            $qb->andWhere('obra.id NOT IN (:obres)');
            $qb->setParameter('obres', $idsObresEnPrograma);
        }
        //Si rep algo per get, s'afegueix a la consulta el filtre per nom.
        if(isset($_GET['nombre'])) {
            $qb->andWhere('obra.nombre LIKE :nom');
            $qb->setParameter('nom', '%'. $_GET['nombre'].'%');
        }
        //Es crea la consulta i es pagina
        $query = $qb->getQuery();
        $obres = $this->paginacio($request, $query, 10);

        $params=array(
            'event'=>$concert,
            'obres'=>$obres
        );
        return $this->render('AppBundle:Default:asignarObraAEvent.html.twig', $params);
    }

    //AFEGIR OBRA EVENT A BBDD
    public function obraEventAction($idEvent, $idObra){
        $em =$this->getDoctrine()->getManager();
        $concert=$em->getRepository('AppBundle:evento')->find($idEvent);
        if (!$concert) {
            $mensaje=array('error'=>'El event seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $obra=$em->getRepository('AppBundle:obra')->find($idObra);
        if (!$obra) {
            $mensaje=array('error'=>'L\'obra seleccionada no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        //Comproba si no eres director
        if (!$this->comprobarDirector() and !$this->isGranted('ROLE_ADMIN')) {
            $mensaje = array('error' => 'No ets director.');
            return ($this->render('AppBundle:Default:error.html.twig', $mensaje));
        }//Comproba si eres director en alguna agrupació
        if ($this->comprobarDirector()){
            //Si eres director comproba que l'agrupació de la que eres director y la agrupació que fa el event siga igual
            $agrs = array();
            foreach((array)$this->comprobarAgrupacionsDelDirector() as $objToca) {
                array_push($agrs, $objToca->getIdAgrupacion()->getNombre());
            }
            if (!in_array($concert->getAgrupacion()->getNombre(), $agrs)){
                $mensaje=array('error'=>'No ets director d\'aquesta agrupació');
                return($this->render('AppBundle:Default:error.html.twig',$mensaje));
            }
        }

        //Si el idObra que li passem está en al Array de Concert->obra ja existirá y donará error.
        foreach ($concert->getObra() as $concertObra){
            if ($concertObra->getId() == $idObra){
                $mensaje=array('error'=>'L\'obra ja està asignada al event indicat.');
                return($this->render('AppBundle:Default:error.html.twig',$mensaje));
            }
        }

        $concert->addObra($obra);
        $em->persist($concert);
        $em->flush();

        $obres=$em->getRepository('AppBundle:obra')->findAll();
        $params=array(
            'idEvent'=>$concert->getId(),
            'concert'=>$concert,
            'obres'=>$obres
        );
        return $this->redirect($this->generateUrl('app_asignarObraAEvent',$params));
    }

    //ELIMINAR OBRA DE EVENT
    public function eliminarObraEventAction($idEvent, $idObra){
        $em =$this->getDoctrine()->getManager();
        $concert=$em->getRepository('AppBundle:evento')->find($idEvent);
        if (!$concert) {
            $mensaje=array('error'=>'El event seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $obra=$em->getRepository('AppBundle:obra')->find($idObra);
        if (!$obra) {
            $mensaje=array('error'=>'L\'obra seleccionada no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        //Comproba si no eres director
        if (!$this->comprobarDirector() and !$this->isGranted('ROLE_ADMIN')) {
            $mensaje = array('error' => 'No ets director.');
            return ($this->render('AppBundle:Default:error.html.twig', $mensaje));
        }//Comproba si eres director en alguna agrupació
        if ($this->comprobarDirector()){
            //Si eres director comproba que l'agrupació de la que eres director y la agrupació que fa el event siga igual
            $agrs = array();
            foreach((array)$this->comprobarAgrupacionsDelDirector() as $objToca) {
                array_push($agrs, $objToca->getIdAgrupacion()->getNombre());
            }
            if (!in_array($concert->getAgrupacion()->getNombre(), $agrs)){
                $mensaje=array('error'=>'No ets director d\'aquesta agrupació');
                return($this->render('AppBundle:Default:error.html.twig',$mensaje));
            }
        }

        $concert->removeObra($obra);
        $em->persist($concert);
        $em->flush();

        $obres=$em->getRepository('AppBundle:obra')->findAll();
        $params=array(
            'idEvent'=>$concert->getId(),
            'concert'=>$concert,
            'obres'=>$obres
        );
        return $this->redirect($this->generateUrl('app_asignarObraAEvent',$params));
    }

    //ASSIGNAR UN COMENTARI A UN EVENT
    public function afegirComentariEventAction($idEvent, $idComent, Request $request){

        $em =$this->getDoctrine()->getManager();
        $event=$em->getRepository('AppBundle:evento')->find($idEvent);
        if (!$event) {
            $mensaje=array('error'=>'El event seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }
        //Si eres músic pero no perteneixes a la agrupació seleccionada
        $toca=$em->getRepository('AppBundle:toca')->findBy(array('idUsuario'=>$this->getUser()->getId(), 'idAgrupacion'=>$event->getAgrupacion()->getId()));
        if (!$toca && !$this->isGranted('ROLE_DIRECTIU')) {
            $mensaje=array('error'=>'No pots afegir comentaris ja que no ets músic d\'aquesta agrupació.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        if(is_null($idComent)){
            $coment = new comentarios();
            $data = new \DateTime();
            $coment->setFecha($data);
            $coment->setLastModify(null);
            $coment->setUsuarios($this->getUser());
            $coment->setEvento($event);
        }else {
            $coment=$em->getRepository('AppBundle:comentarios')->find($idComent);
            $ultimaModif = new \DateTime();
            $coment->setLastModify($ultimaModif);
            $coment->setLastUserModify($this->getUser());

            if ($coment->getUsuarios() != $this->getUser() && !$this->isGranted('ROLE_MODERADOR')){
                $mensaje=array('error'=>'No tens permís per a editar aquest comentari.');
                return($this->render('AppBundle:Default:error.html.twig',$mensaje));
            }
        }

        $form= $this->createForm(new comentariosType(), $coment, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $coment->setTexto($form['texto']->getData());

                $em->persist($coment);
                $em->flush();

                $params['idEvent'] = $idEvent;
                return $this->redirect($this->generateUrl('app_mostrarComentarisEvent', $params));
            }
        }

        return $this->render('AppBundle:Default:formAfegirComentariEvent.html.twig', array('form'=>$form->createView()));
    }

    //ELIMINAR UN COMENTARI DE UN EVENT
    public function eliminarComentariEventAction($idEvent, $idComent){
        $em =$this->getDoctrine()->getManager();
        $event=$em->getRepository('AppBundle:evento')->find($idEvent);
        if (!$event) {
            $mensaje=array('error'=>'El event seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $coment=$em->getRepository('AppBundle:comentarios')->find($idComent);
        if (!$coment) {
            $mensaje=array('error'=>'El comentari seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }
        //Si eres músic pero no perteneixes a la agrupació seleccionada
        $toca=$em->getRepository('AppBundle:toca')->findBy(array('idUsuario'=>$this->getUser()->getId(), 'idAgrupacion'=>$event->getAgrupacion()->getId()));
        if (!$toca && !$this->isGranted('ROLE_DIRECTIU')) {
            $mensaje=array('error'=>'No pots accedir als comentaris ja que no ets músic d\'aquesta agrupació.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }
        foreach ($event->getComentarios() as $result){
            if($result->getEvento() !== $coment->getEvento()){
                $mensaje=array('error'=>'El comentari que intentes borrar no correspon al event seleccionat.');
                return($this->render('AppBundle:Default:error.html.twig',$mensaje));
            }
        }

        //Comproba si el comentari es teu o eres admin
        if ($coment->getUsuarios() != $this->getUser() && !$this->isGranted('ROLE_MODERADOR')){
            $mensaje=array('error'=>'No tens permís per eliminar aquest comentari.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $em->remove($coment);
        $em->flush();

        $comentaris=$em->getRepository('AppBundle:comentarios')->findByEvento($idEvent);
        $params=array(
            'idEvent'=>$event->getId(),
            'event'=>$event,
            'comentaris'=>$comentaris
        );
        return $this->redirect($this->generateUrl('app_mostrarComentarisEvent',$params));
    }

    //MOSTRA ELS COMENTARIS DE UN EVENT
    public function mostrarComentarisEventAction($idEvent, Request $request){
        $em =$this->getDoctrine()->getManager();
        $event=$em->getRepository('AppBundle:evento')->find($idEvent);

        if (!$event) {
            $mensaje=array('error'=>'El event seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }
        //Si eres músic pero no perteneixes a la agrupació seleccionada
        $toca=$em->getRepository('AppBundle:toca')->findBy(array('idUsuario'=>$this->getUser()->getId(), 'idAgrupacion'=>$event->getAgrupacion()->getId()));
        if (!$toca && !$this->isGranted('ROLE_DIRECTIU')) {
            $mensaje=array('error'=>'No pots accedir als comentaris ja que no ets músic d\'aquesta agrupació.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }
        $comentarios = $this->getDoctrine()->getRepository('AppBundle:comentarios');
        $query = $comentarios->createQueryBuilder('comentarios');
        $query->andWhere('comentarios.evento = :evento');
        $query->setParameter('evento', $idEvent);

        if(isset($_GET['text'])) {
            if($_GET['text']!="") {
                $query->andWhere('comentarios.texto LIKE :coment');
                $query->setParameter('coment', '%' . $_GET['text'] . '%');
            }
        }
        $query->orderBy('comentarios.fecha', 'asc');
        $comentarisPaginats = $this->paginacio($request, $query, 5);

        $messageComentBuit = "No hi ha comentaris a l'esdeveniment.";
        if (isset($_GET['text'])) {
            $messageComentBuit = "No s'ha trobat cap comentari que continga: ".$_GET['text'];
        }

        $params=array(
            'event'=>$event,
            'comentaris' => $comentarisPaginats,
            'comentBuit'=> $messageComentBuit
        );
        return $this->render('AppBundle:Default:mostrarComentarisEvent.html.twig', $params);
    }

    //LLISTAR ACTES
    public function llistarActesAction(Request $request){
        //Crea la fexa actual en el mateix formato que esta a la bbdd per a comparar-ho en la consulta
        $data = new \DateTime();
        $dataActual = $data->format('Y-m-d H:i:s');

        //Consulta el QueryBuilder per mostrar els próxims ordenats per fecha
        $em = $this->getDoctrine()->getRepository('AppBundle:evento');
        $qb = $em->createQueryBuilder('evento');
        $qb->andWhere('evento.fechaHora > :fechaActual');
        $qb->andWhere('evento.tipo != :tipo');
        $qb->setParameter('fechaActual', $dataActual);
        $qb->setParameter('tipo', 'Assaig');
        //es necesari possarli un 'alias' a agrupacion_id per a que es puga ordenar per aquest camp en el paginator
        //$qb->leftJoin('evento.agrupacion', 'agrup');
        $query = $qb->getQuery();

        //Primer a partir de la consulta general, trau tots els tipus de events i agrupacions creades per plenar el form.
        $tipus= array();
        foreach($query->getResult() as $result){
            if ($result->getTipo() != 'Assaig'){
                if(!in_array($result->getTipo(), $tipus)){
                    array_push($tipus, $result->getTipo());
                }
            }
        }
        $agrupacions=array();
        foreach($query->getResult() as $result){
            if(!in_array($result->getAgrupacion(), $agrupacions)){
                array_push($agrupacions, $result->getAgrupacion());
            }
        }

        if(isset($_GET['acte'])||isset($_GET['agrupacio'])) {
            if ($_GET['acte'] != "") {
                $qb->andWhere('evento.tipo LIKE :tip');
                $qb->setParameter('tip', $_GET['acte']);
            }
            if ($_GET['agrupacio'] != "") {
                $qb->andWhere('evento.agrupacion = :ag');
                $qb->setParameter('ag', $_GET['agrupacio']);
            }
        }
        $qb->orderBy('evento.fechaHora', 'asc');
        $query = $qb->getQuery();

        $actesPaginats = $this->paginacio($request, $query, 10);
        $params=array(
            'actes'=>$actesPaginats,
            'tipusDacte'=>$tipus,
            'agrupacions'=>$agrupacions
        );

        return $this->render('AppBundle:Default:llistarActes.html.twig', $params);
    }

    //LLISTAR ACTES PASSATS
    public function llistarActesPassatsAction(Request $request){
        //Crea la fexa actual en el mateix formato que esta a la bbdd per a comparar-ho en la consulta
        $data = new \DateTime();
        $dataActual = $data->format('Y-m-d H:i:s');

        //Consulta el QueryBuilder
        $em = $this->getDoctrine()->getRepository('AppBundle:evento');
        $qb = $em->createQueryBuilder('evento');
        $qb->andWhere('evento.fechaHora < :fechaActual');
        $qb->andWhere('evento.tipo != :tipo');
        $qb->setParameter('fechaActual', $dataActual);
        $qb->setParameter('tipo', 'Assaig');
        $qb->orderBy('evento.fechaHora', 'desc');
        $qb->leftJoin('evento.agrupacion', 'agrup');
        $query = $qb->getQuery();

        //Una volta feta la consulta es passa al paginador
        $queryPaginada = $this->paginacio($request, $query, 10);

        $params=array(
            'actes'=>$queryPaginada
        );

        return $this->render('AppBundle:Default:llistarActesPassats.html.twig', $params);
    }

    //LLISTAR CONCERTS PER ALS MUSICS
    public function llistarConcertsMusicAction(Request $request){
        $user = $this->getUser();

        //Crea la fexa actual en el mateix formato que esta a la bbdd per a comparar-ho en la consulta
        $data = new \DateTime();
        $dataActual = $data->format('Y-m-d H:i:s');

        //Consulta el QueryBuilder per mostrar els próxims ordenats per fecha
        $em = $this->getDoctrine()->getRepository('AppBundle:evento');
        $qb = $em->createQueryBuilder('evento');
        $qb->andWhere('evento.fechaHora > :fechaActual');
        $qb->setParameter('fechaActual', $dataActual);
        $qb->orderBy('evento.fechaHora', 'asc');
        $query = $qb->getQuery();

        $actesPaginats = $this->paginacio($request, $query, 10);
        $em =$this->getDoctrine()->getManager();
        $toca=$em->getRepository('AppBundle:toca')->findByIdUsuario($user->getId());

        $params=array(
            'actes'=>$actesPaginats,
            'toca'=>$toca
        );

        return $this->render('AppBundle:Default:llistarConcertsMusic.html.twig', $params);
    }

    //MOSTRAR EVENTS AL CALENDARI
    public function eventsCalendariAction(Request $request) {
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $result = array();
        $dades=array();

        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:evento');
        //Es necessari el switch, ja que en IF pot entrar en ROLE_DIRECTIU i despres en ROLE_MUSIC, en switch puc gastar break
        switch($this){
            case $this->isGranted('ROLE_DIRECTIU'):
                //Si eres directiu mostra tots els events.
                $concerts = $repo->findAll();
                break;
            case $this->isGranted('ROLE_MUSIC'):
                // Si eres music mostra tots els events afegint la sentencia consultaEventSegonsMusic()
                $qb = $repo->createQueryBuilder('evento');
                $qb = $this->consultaEventSegonsMusic($qb);
                $query = $qb->getQuery();
                $concerts = $query->getResult();
                break;
            default:
                //Si eres usuari sense permisos, mostra tots els events que no siguen assatjos.
                $qb = $repo->createQueryBuilder('evento');
                $qb->andWhere('evento.tipo != :tipo');
                $qb->setParameter('tipo', 'Assaig');
                $query = $qb->getQuery();
                $concerts=$query->getResult();
        }

        foreach($concerts as $concert) {
            //Aquesta consulta es per saber si ni ha més d'un acte el mateix día que el concert indicat.
            $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:evento');
            $qb = $repo->createQueryBuilder('evento');
            $qb->andWhere('evento.fechaHora like :fecha');
            $qb->setParameter('fecha', $concert->getFechaHora()->format('Y-m-d') . '%');
            //Degut a l'herencia de rols es controlará...
            switch($this){
                //si eres directiu, no afeguixes res a la consulta per a que ho mostre tot.
                case $this->isGranted('ROLE_DIRECTIU'):
                    break;
                //si eres music, afeguix les sentencies vorer tots els actes - assaigs que no siguen la seua agrup
                case $this->isGranted('ROLE_MUSIC'):
                    $qb = $this->consultaEventSegonsMusic($qb);
                    break;
                //si eres usuari, afeguix la sentencia de no vorer els assaigs.
                default:
                    $qb->andWhere('evento.tipo != :tipo');
                    $qb->setParameter('tipo', 'Assaig');
            }
            $query = $qb->getQuery();
            $eventsMateixDia=$query->getResult();

            $classMultiple = null;
            //Si només ni ha un acte creará la ventana modal d'aquesta forma.
            if (count($eventsMateixDia) == 1){
                $dades['date']=$concert->getFechaHora()->format('Y-m-d');
                $dades['title']='<strong>'.$concert->getTipo().' '.$concert->getNombre().'</strong>, a carrec de la '.
                    $concert->getAgrupacion()->getNombre();
                $dades['body']="L'esdeveniment tindrà lloc en: ".$concert->getLugar()."<br> Citats a l'hora: "
                    .$concert->getFechaHora()->format('H:i');
                $rutaActual = $request->getUri();
                $rutaPartida =explode('/eventsCalendari',$rutaActual);
                $url = $rutaPartida[0].'/mostrarEvent/'.$concert->getId();
                $dades['footer']="<a href=".$url.">Més info.</a>";

                switch($concert->getTipo()){
                    case 'Certamen':
                        $dades['classname']= 'certamen';
                        break;
                    case 'Concert':
                        $dades['classname']= 'concert';
                        break;
                    case 'Acte de carrer':
                        $dades['classname']= 'acte';
                        break;
                    case 'Assaig':
                        $dades['classname']= 'assaig';
                        break;
                }
            } // En cas de haberne més de un, la ventana modal afegirá tots els events d'eixe día
            else {
                $dades['date']=$concert->getFechaHora()->format('Y-m-d');
                $dades['title']="Esdeveniments pogramats per al ".$concert->getFechaHora()->format('d-m-Y');
                $dades['body']='';
                $dades['footer']='';
                foreach ($eventsMateixDia as $event){
                    $dades['body'].='<strong>'.$event->getTipo().' '.$event->getNombre().'</strong>, a carrer de la '
                        .$event->getAgrupacion()->getNombre().'.</br>';
                    $dades['body'].="L'esdeveniment tindrà lloc en: ".$event->getLugar()."<br> Citats a l'hora: "
                        .$event->getFechaHora()->format('H:i').'.</br>';
                    if ($event !== end($eventsMateixDia)){
                        $dades['body'].='</br></br>';
                    }

                    $rutaActual = $request->getUri();
                    $rutaPartida =explode('/eventsCalendari',$rutaActual);
                    $url = $rutaPartida[0].'/mostrarEvent/'.$event->getId();
                    $dades['footer'].="<a href=".$url.">Més info ".$event->getTipo().' '.$event->getNombre()."</a>";
                    if ($event !== end($eventsMateixDia)){
                        $dades['footer'].=" &nbsp;/&nbsp;";
                    }

                    switch($event->getTipo()){
                        case 'Certamen':
                            $classMultiple = 'certamen';
                            break;
                        case 'Concert':
                            if ($classMultiple != 'certamen'){
                                $classMultiple = 'concert';
                            }
                            break;
                        case 'Acte de carrer':
                            if (($classMultiple != 'certamen') and ($classMultiple != 'concert')){
                                $classMultiple = 'acte';
                            }
                            break;
                        case 'Assaig':
                            if ($classMultiple == null){
                                $classMultiple= 'assaig';
                            }
                            break;
                    }
                }

                $dades['classname']= $classMultiple;
            }

            $dades['badge']=true;
            array_push($result, $dades);
        }
        return new JsonResponse($result);
    }

    //ASISTENCIA A EVENT SI
    public function asistenciaSiAction($idEvent, $idUsuari){
        $em =$this->getDoctrine()->getManager();
        $asist=$em->getRepository('AppBundle:asiste')->findBy(array('eventoId'=>$idEvent,'usuarioId'=>$idUsuari));
        $asist[0]->setAsiste('Sí');
        $em->persist($asist[0]);
        $em->flush();

        $params['id']=$idEvent;
        return $this->redirect($this->generateUrl('app_asistenciaAEvent',$params));
    }

    //ASISTENCIA A EVENT NO
    public function asistenciaNoAction($idEvent, $idUsuari){
        $em =$this->getDoctrine()->getManager();
        $asist=$em->getRepository('AppBundle:asiste')->findBy(array('eventoId'=>$idEvent,'usuarioId'=>$idUsuari));
        $asist[0]->setAsiste('No');
        $em->persist($asist[0]);
        $em->flush();

        $params['id']=$idEvent;
        return $this->redirect($this->generateUrl('app_asistenciaAEvent',$params));
    }

    //ASISTENCIA A EVENT (MOSTRA ELS USUARIS QUE VAN, NO VAN I ESTAN PER CONFIRMAR)
    public function asistenciaAEventAction($id){
        $em =$this->getDoctrine()->getManager();
        $concert=$em->getRepository('AppBundle:evento')->find($id);
        //conculta per saber els musics que es troben a l'agrupació del event
        $dql = "SELECT toca FROM AppBundle:toca toca WHERE toca.idAgrupacion = :concert order by toca.idUsuario asc";
        $query = $em->createQuery($dql);
        $query->setParameter('concert', $concert->getAgrupacion()->getId());
        $agrupacio = $query->getResult();
        //array amb els ids dels musics que toquen a l'agrupació del event
        $idUsuaris=array();
        foreach($agrupacio as $result){
            if ($result->getIdUsuario()->getBaixa() == 'No'){
                array_push($idUsuaris, $result->getIdUsuario());
            }
        }
        //consulta per filtrar en la taula asiste els usuaris que toquen a l'agrupació on es desarrolla el event
        //$usuaris=$em->getRepository('AppBundle:asiste')->findByEventoId($id);
        $asistencia=$em->getRepository('AppBundle:asiste');
        $qb=$asistencia->createQueryBuilder('asist');
        $qb->andWhere('asist.eventoId = :concert')
            ->andWhere('asist.usuarioId IN (:idUsers)');
        $qb->setParameter('concert', $id);
        $qb->setParameter('idUsers', $idUsuaris);
        $qb->orderBy('asist.asiste', 'desc');
        $query=$qb->getQuery();
        $asistencia=$query->getResult();
        $params=array(
            'asist'=>$asistencia,
            'event'=>$concert,
            'agrupacio'=>$agrupacio
        );
        return $this->render('AppBundle:Default:asistenciaAEvent.html.twig', $params);
    }


//-------------------------------ZONA MEMBRESIA-----------------------------
    //LLISTAR CORDES DE LES ENTITATS
    public function llistarCordesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $agrupacions = $em->getRepository('AppBundle:agrupacion')->findAll();

        //ids dels usuaris que están de baixa.
        $idUsuarisNoBaixa = array();
        $usuarisBaixa = $em->getRepository('AppBundle:usuarios')->findBy(array('baixa' => 'No'));
        foreach ($usuarisBaixa as $idUs) {
            array_push($idUsuarisNoBaixa, $idUs);
        }
        $toca = null;
        $errorMensatge = null;

        $em = $this->getDoctrine()->getRepository('AppBundle:toca');
        $qb = $em->createQueryBuilder('toca');
        $qb->andWhere('toca.idUsuario IN (:ids_usua)')
            ->setParameter('ids_usua', $idUsuarisNoBaixa);
        if (isset($_GET['instrument']) || isset($_GET['agrupacio'])) {
            if ($_GET['instrument'] != "") {
                $qb->andWhere('toca.idInstrumento = :instr');
                $qb->setParameter('instr', $_GET['instrument']);
            }
            if ($_GET['agrupacio'] != "") {
                $qb->andWhere('toca.idAgrupacion = :agrupa');
                $qb->setParameter('agrupa', $_GET['agrupacio']);
            } else {
                $errorMensatge = "Per favor, selecciona una agrupació.";
            }

            $qb->groupBy('toca.idInstrumento, toca.idUsuario');
            $qb->orderBy('toca.capDeCorda', 'desc');

            $query = $qb->getQuery();

            $toca = $this->paginacio($request, $query, 15);
        }

        $params = array(
            'agrupacions' => $agrupacions,
            'toca' => $toca,
            'error' => $errorMensatge
        );

        return $this->render('AppBundle:Default:llistarCordes.html.twig', $params);
    }

    //LLISTAR USUARIS INACTIUS DE LA APP
    public function llistarInactiusAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $consultaUsuarisBaixa = $em->getRepository('AppBundle:usuarios')->findBy(array('baixa' => 'Sí'));
        $usuarisBaixa = array();
        $usuaris = null;

        if (isset($_GET['rol'])) {
            if ($_GET['rol'] != "") {
                if ($_GET['rol'] == "musics") {
                    foreach ($consultaUsuarisBaixa as $us) {
                        if (in_array('ROLE_MUSIC',$us->getRoles()))
                        array_push($usuarisBaixa, $us);
                    }
                }else {
                    foreach ($consultaUsuarisBaixa as $us) {
                        if (in_array('ROLE_DIRECTIU',$us->getRoles()))
                            array_push($usuarisBaixa, $us);
                    }
                }
            }

            $usuaris = $this->paginacio($request, $usuarisBaixa, 10);
        }


        $params = array(
            'users' => $usuaris
        );

        return $this->render('AppBundle:Default:llistarInactius.html.twig', $params);
    }

    //LLISTAR DIRECTIUS DE LA APP
    public function llistarDirectiusAction(Request $request)
    {
        $errorMensatge = null;
        $result = null;

        $em = $this->getDoctrine()->getRepository('AppBundle:usuarios');
        $qb = $em->createQueryBuilder('directiu');
        $qb->andWhere('directiu.baixa NOT LIKE :baixa')
            ->setParameter('baixa', 'Sí');
        if (isset($_GET['carrec'])) {
            if ($_GET['carrec'] != "") {
                switch ($_GET['carrec']) {
                    case 'Directiu':
                        $rol = 'ROLE_DIRECTIU';
                        break;
                    case 'Magatzem':
                        $rol = 'ROLE_MAGATZEM';
                        break;
                    case 'Arxiver':
                        $rol = 'ROLE_ARXIVER';
                        break;
                    case 'Moderador':
                        $rol = 'ROLE_MODERADOR';
                        break;
                    case 'Tresorer':
                        $rol = 'ROLE_TRESORER';
                        break;
                    case 'Secretari':
                        $rol = 'ROLE_SECRETARI';
                        break;
                    case 'Administrador':
                        $rol = 'ROLE_ADMIN';
                        break;
                }
                $idsDirectius = array();
                $usuaris = $em->findAll();
                foreach ($usuaris as $users) {
                    if (in_array($rol, $users->getRoles())) {
                        array_push($idsDirectius, $users->getId());
                    }
                }

                $qb->andWhere('directiu.id IN (:ids_users)');
                $qb->setParameter('ids_users', $idsDirectius);
                $qb->orderBy('directiu.nombre, directiu.apellidos', 'asc');
            } else {
                $errorMensatge = "Per favor, selecciona una agrupació.";
            }

            $query = $qb->getQuery();
            $result = $this->paginacio($request, $query, 10);
        }

        $params = array(
            'directius' => $result,
            'error' => $errorMensatge
        );
        return $this->render('AppBundle:Default:llistarDirectius.html.twig', $params);
    }


//------------------------------- per asignar----------------------------------

    //Aquesta comprobació retornará true si eres director i false si no
    public function comprobarDirector(){
        if ($this->isGranted('ROLE_MUSIC')){
            $userId = $this->getUser()->getId();

            $em =$this->getDoctrine()->getManager();
            $toca=$em->getRepository('AppBundle:toca')->findByIdUsuario($userId);

            $instrum = array();
            foreach($toca as $result){
                array_push($instrum, $result->getIdInstrumento()->getNombre());

                if (in_array('Director', $instrum)){
                    return true;
                }
                return false;
            }
        }
    }

    //Aquesta comprobació retornará un array en el obj Toca de les agrupacions on el usuari es director
    public function comprobarAgrupacionsDelDirector(){
        if ($this->comprobarDirector()){
            $em =$this->getDoctrine()->getManager();
            $toca=$em->getRepository('AppBundle:toca')->findByIdUsuario($this->getUser()->getId());

            $agrs = array();
            foreach ($toca as $comprobacio) {
                if ($comprobacio->getIdInstrumento()->getNombre() == 'Director'){
                    array_push($agrs, $comprobacio);
                }
            }
            return $agrs;
        }
    }

    //Aquesta comprobació retornará un array bidimensional amb les agrupacions i instrument que toca el usuari
    public function comprobarMusicAgrupacio(){
        if ($this->isGranted('ROLE_MUSIC')){
            $userId = $this->getUser()->getId();

            $em =$this->getDoctrine()->getManager();
            $toca=$em->getRepository('AppBundle:toca')->findByIdUsuario($userId);

            $music = array();
            foreach ($toca as $result){
                array_push($music, array($result->getIdInstrumento(), $result->getIdAgrupacion()));
            }

            return $music;
        }
    }

    //AJAX per plenar la veu segons l'instrument a la vista AfegirPartitura
    public function ajaxPartituraVeuAction(Request $request) {
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        // Agafa el valor pasat per post (ajax?instrument_id=clarinet..)
        $nomInstr = $request->query->get('instrument_id');
        $result = array();

        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:instrumento');
        // Retorna un array (sols d'una posicio) amb l'instrument amb eixe nom
        $instruments = $repo->findByNombre($nomInstr);
        //recorre l'array duna posicio per agafar la veu partintla per la "/" i posteriorment l'afig a $result (index/valor)
        foreach ($instruments as $instrument) {
            $veuPartida=explode('/',$instrument->getVoz());
            foreach($veuPartida as $veupartidaResult){
                $result[$veupartidaResult]= $veupartidaResult;
            }

        }
        return new JsonResponse($result);
    }

    //AJAX per plenar el instrument segons l'agrupació a la vista llistarCordes
    public function ajaxLlistarCordesAction(Request $request) {
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        // Agafa el valor pasat per post (ajax?agrupacio_id=clarinet..)
        $idAgrup = $request->query->get('agrupacio_id');
        $result = array();

        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:agrupacion');
        // Retorna un array (sols d'una posicio) amb l'instrument amb eixe nom
        $agrupacio = $repo->find($idAgrup);
        //recorre l'array duna posicio per agafar la veu partintla per la "/" i posteriorment l'afig a $result (index/valor)
        $em = $this->getDoctrine()->getManager()->getRepository('AppBundle:toca');
        $toca = $em->findBy(array('idAgrupacion'=>$agrupacio));
        $result[''] = '';
        foreach ($toca as $compet) {
            $result[$compet->getIdInstrumento()->getNombre()] = $compet->getIdInstrumento()->getId();
        }
        return new JsonResponse($result);
    }

    //AJAX per modificar el camp instrument segons la agrupacio si eres cap de corda
    public function ajaxCompetenciesAction(Request $request) {
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        // Agafa el valor pasat per post (ajax?instrument_id=clarinet..)
        $agrupacionId= $request->query->get('agrupacio_id');
        $result = array();

        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:toca');
        // Retorna un array (sols d'una posicio) amb l'instrument amb eixe nom
        $fila = $repo->findBy(array('idUsuario'=>$this->getUser()->getId(),'idAgrupacion'=>$agrupacionId));
        $totsInstr = $this->getDoctrine()->getManager()->getRepository('AppBundle:instrumento')->findAll();
        if ($fila[0]->getIdInstrumento()->getNombre() == 'Director'){
            foreach ($totsInstr as $instr){
                $result[$instr->getId()]= $instr->getNombre();
            }
        }else {
            foreach ($fila as $instrument) {
                $result[$instrument->getIdInstrumento()->getId()]= $instrument->getIdInstrumento()->getNombre();
            }
        }

        return new JsonResponse($result);
    }

    //Paginació
    public function paginacio(Request $request, $query, $limitPerPage){
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('pagina', 1)/*page number*/,
            $limitPerPage/*limit per page*/
        );

        return $pagination;
    }

    //Demana una queryBuilder(de evento) i afegira la sentencia que el music toque en eixa agrupació o el event no siga un assaig.
    //D'aquesta manera retornará els events on toque i on no toque sempre que no siguen assatjos.
    public function consultaEventSegonsMusic($queryBuilder){
        if ($this->isGranted('ROLE_MUSIC')) {
            $userId = $this->getUser()->getId();

            $em = $this->getDoctrine()->getManager();
            $toca = $em->getRepository('AppBundle:toca')->findByIdUsuario($userId);

            $idAgrup = array();
            foreach ($toca as $competencies) {
                array_push($idAgrup, $competencies->getIdAgrupacion()->getId());
            }
            $queryBuilder->andWhere('evento.agrupacion IN (:agrup) or evento.tipo != :tipo');
            $queryBuilder->setParameter('agrup', $idAgrup);
            $queryBuilder->setParameter('tipo', 'Assaig');

            return $queryBuilder;
        }
    }

    //widget
    public function widgetAction(Request $request) {
        $resultEvents = array();
        $dadesEvents= array();
        $fecha = new \DateTime();
        $em= $this->getDoctrine()->getRepository('AppBundle:evento');
        $qb = $em->createQueryBuilder('evento');
        switch($this){
            case $this->isGranted('ROLE_DIRECTIU'):
                break;
            case $this->isGranted('ROLE_MUSIC'):
                // Si eres music mostra tots els events afegint la sentencia consultaEventSegonsMusic()
                $qb = $this->consultaEventSegonsMusic($qb);
                break;
            default:
                //Si eres usuari sense permisos, mostra tots els events que no siguen assatjos.
                $qb->andWhere('evento.tipo != :tipo');
                $qb->setParameter('tipo', 'Assaig');
        }
        $qb->andWhere('evento.fechaHora > :fecha');
        $qb->setParameter('fecha', $fecha);
        $qb->orderBy('evento.fechaHora');
        $qb->setMaxResults( 5 );
        $results=$qb->getQuery()->getResult();

        $i=0;
        foreach($results as $fila){
            $dadesEvents['tipo']=$fila->getTipo();
            $dadesEvents['nom']=$fila->getNombre();
            $dadesEvents['lloc']=$fila->getLugar();
            $dadesEvents['agrupacio']=$fila->getAgrupacion()->getNombre();
            $dadesEvents['fexa']=$fila->getFechaHora()->format('d-m-y');
            $dadesEvents['hora']=$fila->getFechaHora()->format('H:i');
            $dadesEvents['id']=$fila->getId();
            $resultEvents[$i]=$dadesEvents;
            $i++;
        }


        $resultNoticia = array();
        $dadesNoticia= array();
        $em= $this->getDoctrine()->getRepository('AppBundle:noticia');
        $query = $em->createQueryBuilder('noticia');
        $query->orderBy('noticia.fecha', 'desc');
        $query->setMaxResults( 5 );
        $resultsNoticia=$query->getQuery()->getResult();
        $i=0;
        foreach($resultsNoticia as $fila){
            $dadesNoticia['id']=$fila->getId();
            $dadesNoticia['titol']=$fila->getTitulo();
            $dadesNoticia['fexa']=$fila->getFecha()->format('d-m-Y');
            $dadesNoticia['font']=$fila->getFuente();
            $dadesNoticia['url']=$fila->getUrl();
            $resultNoticia[$i]=$dadesNoticia;
            $i++;
        }


        $noticiesIEvents=array();
        $noticiesIEvents['noticies']=$resultNoticia;
        $noticiesIEvents['events']=$resultEvents;
        $tot=new JsonResponse($noticiesIEvents);
        return $tot;

    }

    //redimensiona les imatges a la mitad de tamany i comprimeix el pes.
    public function redimensionaImatge($archivo){
        //REVISAR EL FORMAT DE LA IMATGE!!!Getimagesize() torna un array, la posició 2 conte el tipo de img
        $format=getimagesize($archivo);
        if($format[2]== IMAGETYPE_JPEG ){
            $imgOriginal=imagecreatefromjpeg($archivo);
        }
        else if($format[2]== IMAGETYPE_PNG){
            $imgOriginal=imagecreatefrompng($archivo);
        }

        $ample = intval(imagesx($imgOriginal) / 2);
        $alt = intval(imagesy($imgOriginal) / 2);

        $imageNova=imagecreatetruecolor($ample, $alt);
        imagecopyresampled(
            $imageNova, $imgOriginal, 0, 0, 0, 0,
            $ample, $alt,
            imagesx($imgOriginal), imagesy($imgOriginal));

        return $imageNova;
    }
}
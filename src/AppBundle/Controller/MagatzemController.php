<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\insMagatzemType;
use AppBundle\Entity\insMagatzem;


class MagatzemController extends DefaultController{
    //----------------ZONA PRESTEC DE INSTRUMENTS--------------------
    //AFEGIR INSTRUMENT
    public function afegirInstrumentAction(Request $request, $id){
        $em =$this->getDoctrine()->getManager();
        if(is_null($id)){
            $instrument=new insMagatzem();
        }
        else{
            $instrument=$em->getRepository('AppBundle:insMagatzem')->find($id);
        }
        $form= $this->createForm(new insMagatzemType($em), $instrument, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST') {

            $form->bind($request);
            if($form->isValid()){
                $em->persist($instrument);
                $em->flush();
                return $this->redirect($this->generateUrl('app_llistarPrestecs'));
            }
        }
        return $this->render('AppBundle:Default:formAfegirInstrumentAMagat.html.twig', array('form'=>$form->createView()));
    }

    //MOSTRAR UN INTRUMENT DEL MAGATZEM AMB DETALL
    public function mostrarInstrumentMagatzemAction($id){
        $em =$this->getDoctrine()->getManager();
        $instrumentMag=$em->getRepository('AppBundle:insMagatzem')->find($id);
        if (!$instrumentMag) {
            $mensaje=array('error'=>'L\'instrument seleccionat no existeix.');
            return($this->render('AppBundle:Default:error.html.twig',$mensaje));
        }

        $params=array(
            'instrument'=>$instrumentMag
        );

        return $this->render('AppBundle:Default:mostrarInstrumentMagatzem.html.twig', $params);
    }

    //LLISTAR USUARIS INSTRUMENTS AL MAGATZEM
    public function llistarPrestecsAction(Request $request){
        $em =$this->getDoctrine()->getManager();
        $prestecs= $em->getRepository('AppBundle:insMagatzem');
        $qb = $prestecs->createQueryBuilder('instruments');

        if(isset($_GET['instrum'])||isset($_GET['serie'])) {
            if ($_GET['instrum'] != "") {
                $qb->andWhere('instruments.nombre LIKE :nom_instr')
                    ->setParameter('nom_instr', '%' . $_GET['instrum'] . '%');
            }
            if ($_GET['serie'] != "") {
                $qb->andWhere('instruments.numSerie LIKE :num_Ser')
                    ->setParameter('num_Ser', $_GET['serie']);
            }
        }
        $qb->orderBy('instruments.nombre', 'asc');
        $query = $qb->getQuery();

        $prestecs = $this->paginacio($request, $query, 10);


        $params=array(
            'prestec'=>$prestecs,
        );
        return $this->render('AppBundle:Default:llistarPrestecs.html.twig', $params);
    }

    //ASIGNAR UN USUARI A UN INSTRUMENT PRESTAT
    public function usuariPrestecAction(Request $request, $id){
        $em =$this->getDoctrine()->getManager();
        $instrument= $em->getRepository('AppBundle:insMagatzem')->find($id);
        $nom=$instrument->getNombre();
        $serie=$instrument->getNumSerie();
        $estat=$instrument->getEstado();
        $observacions=$instrument->getObservaciones();
        $form= $this->createForm(new insMagatzemType($em), $instrument, array('csrf_protection' => false));
        if($request->server->get('REQUEST_METHOD')=='POST') {
            $form->bind($request);
            if($form->isValid()){
                $usu=$form['usuarios']->getData();
                //Busca el usuari amb el id indicat
                $usuarios=$em->getRepository('AppBundle:usuarios')->find($usu);
                //Es pasa l'usuari com a paràmetre per a posar-lo en el arraylist
                $instrument->setUsuarios($usuarios);
                //S'han de tornar a posar els valors dels setters perque al fer el persist sols am l user es borren les dades.
                $instrument->setNombre($nom);
                $instrument->setNumSerie($serie);
                $instrument->setEstado($estat);
                $instrument->setObservaciones($observacions);
                $em->persist($instrument);
                $em->flush();
                return $this->redirect($this->generateUrl('app_llistarPrestecs'));
            }
        }
        return $this->render('AppBundle:Default:formPrestecUsuari.html.twig',array('form'=>$form->createView(), 'instrument'=>$instrument));
    }

    //ELIMINAR PRESTEC USUARI
    public function eliminarPrestecAction($id){
        $em =$this->getDoctrine()->getManager();
        $instrument= $em->getRepository('AppBundle:insMagatzem')->find($id);
        $instrument->setUsuarios(null);
        $em->persist($instrument);
        $em->flush();
        return $this->redirect($this->generateUrl('app_llistarPrestecs'));
    }

    //ELIMINAR INSTRUMENT PRESTEC
    public function eliminarInstrumentAction($id){
        $em =$this->getDoctrine()->getManager();
        $instrument= $em->getRepository('AppBundle:insMagatzem')->find($id);
        $em->remove($instrument);
        $em->flush();
        return $this->redirect($this->generateUrl('app_llistarPrestecs'));
    }
}

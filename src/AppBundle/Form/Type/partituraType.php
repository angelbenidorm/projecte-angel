<?php

namespace AppBundle\Form\Type;
use Doctrine\DBAL\Types\StringType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;


class partituraType extends AbstractType
{
    private $em;
    public function __construct($em){
        $this->em=$em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder->add('pdf', FileType::class);
        //CRIDA AL METODE onPreSetData()
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        //CRIDA AL METODE onPreSubmit()
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }

    //Metode que s'executa sols accedir a afegirPartitura per a carregar els instruments en el combobox crida a addElements
    function onPreSetData(FormEvent $event) {
        $account = $event->getData();
        $form = $event->getForm();
        $instrument = $account->getInstrumento();
        $this->addElementsInstrument($form, $instrument);
    }

    protected function addElementsInstrument(FormInterface $form, $instrumento) {
        $instruments=$this->em->getRepository('AppBundle:instrumento')->findAll();
        $ins=array();
        $veu=array();

        foreach($instruments as $result) {
            $ins[$result->getNombre()] = $result->getNombre();
        }
        $form->add('instrumento', ChoiceType::class, array(
            'attr'=>array('class'=>'form-control'),
            'mapped' => false,
            'choices'  => $ins,
            'empty_value' => 'Selecciona un instrument',
        ));

        //ACI DEU DE ANAR LA VEU DEL INSTRUMENT SELECCIONAT (Variable $instrumento pasat per parametre al metode)
        $form->add('veu', ChoiceType::class, array(
            'attr'=>array('class'=>'form-control'),
            'mapped' => false,
            'empty_value' => 'Selecciona la veu del instrument',
            'choices' => $veu,
        ));
    }

    // AL PULSAR POST Agafa el valor de la veu del combobox i crida a la funci� addElementsVeu() pasantli la veu en forma de array
    function onPreSubmit(FormEvent $event) {
        $arrayVeu=array();
        $account = $event->getData();
        $form = $event->getForm();
        $instrumentVeu = $account['veu'];
        $arrayVeu[$instrumentVeu]=$instrumentVeu;
        $this->addElementsVeu($form, $arrayVeu);
    }

    protected function addElementsVeu(FormInterface $form, $veu) {

        $form->add('veu', ChoiceType::class, array(
            'attr'=>array('class'=>'form-control'),
            'mapped' => false,
            'choices' => $veu,
        ));

    }

    public function getName(){
        return 'partitura';
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class'=>'AppBundle\Entity\partitura',
        );
    }
/*
        $instruments=$this->em->getRepository('AppBundle:instrumento')->findAll();
        $ins=array();
        $veu=array();

        foreach($instruments as $result) {
            $ins[$result->getNombre()] = $result->getNombre();
        }


        $builder->add('instrumento', ChoiceType::class, array(
                        'attr'=>array('class'=>'form-control'),
                        'mapped' => false,
                        'choices'  => $ins
                    ))

            ->add('voz', ChoiceType::class, array(
                'attr' => array('class' => 'form-control'),
                'mapped' => false,
                'choices' => array('Solo' => 'Solo',
                                    '1'=>'1',
                                    '2'=>'2',
                                    '3'=>'3')
            ))
            ->add('pdf', FileType::class);
    }

    public function getName(){
        return 'partitura';
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class'=>'AppBundle\Entity\partitura',
        );
    }*/
}
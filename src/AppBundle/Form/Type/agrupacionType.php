<?php
namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class agrupacionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('nombre', 'text', array(
                'attr'=>array('class'=>'form-control'))
        );
    }

    public function getName(){
        return 'agrupacion';
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class'=>'AppBundle\Entity\agrupacion',
        );
    }
}
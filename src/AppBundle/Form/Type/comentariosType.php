<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class comentariosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder->add('texto', TextareaType::class, array(
                'attr' => array('class' => 'form-control'),
            ));
    }

    public function getName(){
        return 'comentarios';
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class'=>'AppBundle\Entity\comentarios',
        );
    }
}
<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class usuariosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('nombre', 'text', array(
                'attr'=>array('class'=>'form-control'))
        )
            ->add('apellidos', 'text', array(
                    'attr'=>array('class'=>'form-control'))
            )
            ->add('username', 'text', array(
                    'attr'=>array('class'=>'form-control'))
            )
            ->add('password',RepeatedType::class, array(
                'type'=>PasswordType::class,
                'invalid_message'=>'Les contrasenyes no coincideixen',
                'options'=>array('attr'=>array('class'=>'form-control')),
                'required'=>true,
                'first_options'=>array('label'=>' '),
                'second_options'=>array('label'=>'Repetix el password'),

            ))
            ->add('telefono', 'text', array(
                    'attr'=>array('class'=>'form-control'))
            )
            ->add('isBaixa',CheckboxType::class, array(
                    'label'    => 'Es troba inactiu actualment?',
                    'required' => false,
                    'mapped'=>false)
            )
            ->add('imagenPerfil', FileType::class,array(
                'label'=> 'Imatge de Perfil',
                'data_class' => null,
                'mapped'=>false
            ))
            ->add('insMagatzem', ChoiceType::class, array(
                'choices'  => array(
                    'clarinet' => 'Clarinet',
                    'flauta' => 'Flauta',
                    'saxo' => 'Saxo',
                ),
                'mapped'=>false
            ))


            ->add('music',CheckboxType::class, array(
                    'label'    => 'Music',
                    'required' => false,
                    'mapped'=>false)
            )
            ->add('arxiver',CheckboxType::class, array(
                    'label'    => 'Arxiver',
                    'required' => false,
                    'mapped'=>false)
            )

            ->add('directiu',CheckboxType::class, array(
                    'label'    => 'Directiu',
                    'required' => false,
                    'mapped'=>false)
            )
            ->add('magatzem',CheckboxType::class, array(
                    'label'    => 'Magatzem',
                    'required' => false,
                    'mapped'=>false)
            )
            ->add('moderador',CheckboxType::class, array(
                    'label'    => 'Moderador',
                    'required' => false,
                    'mapped'=>false)
            )
            ->add('president',CheckboxType::class, array(
                    'label'    => 'President',
                    'required' => false,
                    'mapped'=>false)
            )
            ->add('tresorer',CheckboxType::class, array(
                    'label'    => 'Tresorer',
                    'required' => false,
                    'mapped'=>false)
            )
            ->add('secretari',CheckboxType::class, array(
                    'label'    => 'Secretari',
                    'required' => false,
                    'mapped'=>false)
            )
            ->add('administrador',CheckboxType::class, array(
                    'label'    => 'Administrador',
                    'required' => false,
                    'mapped'=>false)
            );

    }

    public function getName(){
        return 'usuarios';
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class'=>'AppBundle\Entity\usuarios',
        );
    }
}
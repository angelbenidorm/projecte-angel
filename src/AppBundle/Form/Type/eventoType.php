<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class eventoType extends AbstractType
{
    private $em;
    public function __construct($agrupacionsComboBox){
        $this->em=$agrupacionsComboBox;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder->add('nombre', 'text', array(
                'attr'=>array('class'=>'form-control'))
        )
            ->add('tipo', ChoiceType::class, array(
                'attr' => array('class' => 'form-control'),
                'mapped' => false,
                'choices' => array('Concert' => 'Concert',
                    'Assaig'=>'Assaig',
                    'Acte de carrer'=>'Acte de carrer',
                    'Certamen'=>'Certamen')
            ))
            ->add('fechaHora', DateTimeType::class, array(
                'widget'=>'single_text',
                'format'=>"dd/MM/yyyy hh:mm",
            ))
            ->add('lugar', 'text', array(
                'attr'=>array('class'=>'form-control')
            ))
            ->add('agrupacion', ChoiceType::class, array(
                'attr'=>array('class'=>'form-control'),
                'mapped' => false,
                'choices'  => $this->em
            ));
    }

    public function getName(){
        return 'evento';
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class'=>'AppBundle\Entity\evento',
        );
    }
}
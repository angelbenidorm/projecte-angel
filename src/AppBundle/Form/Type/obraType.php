<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class obraType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('nombre', 'text', array(
                'attr'=>array('class'=>'form-control'))
            )
            ->add('compositor', 'text', array(
                    'attr'=>array('class'=>'form-control'))
            )
            ->add('genero', ChoiceType::class, array(
                'choices'  => array(
                    'Concert' =>'Concert',
                    'Obertura' => 'Obertura',
                    'Suite' => 'Suite',
                    'Fanfàrria'=>'Fanfàrria',
                    'Zarzuela' => 'Zarzuela',
                    'Pasdoble' => 'Pasdoble',
                    'Marxa mora' => 'Marxa mora',
                    'Marxa cristiana' => 'Marxa cristiana',
                    'Precessió' => 'Processió',
                    'Arranjament bandeta' => 'Arranjament bandeta',
                )
            ))
            ->add('arreglista', 'text', array(
                    'attr'=>array('class'=>'form-control'))
            )
            ->add('descripcion', TextareaType::class, array(
                'attr' => array('class' => 'form-control'),
            ))
            ->add('video', 'text', array(
                    'attr'=>array('class'=>'form-control'))
            );
    }

    public function getName(){
        return 'obra';
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class'=>'AppBundle\Entity\obra',
        );
    }
}
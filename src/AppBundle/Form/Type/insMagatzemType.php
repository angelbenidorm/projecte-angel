<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class insMagatzemType extends AbstractType
{
    private $em;
    public function __construct($em){
        $this->em=$em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options){
        $usu=$this->em->getRepository('AppBundle:usuarios')->findAll();
        $us=array();

        foreach($usu as $usuario){
            //ES PLENA LARRAY AMB LES DADES DE CADA USUARI TENINT EN COMPTE QUE EL CHOICETYPE UTILITZA EL FORMAT: VALOR/VALOR A MOSTRAR
            $us[$usuario->getId()]=$usuario->getNombre().' '.$usuario->getApellidos();
        }
        $builder->add('nombre', 'text', array(
                    'attr'=>array('class'=>'form-control'))
        )
            ->add('numSerie', 'text', array(
                    'attr'=>array('class'=>'form-control'))
            )
            ->add('estado', 'text', array(
                    'attr'=>array('class'=>'form-control'))
            )
            ->add('observaciones', TextareaType::class, array(
                    'attr' => array('class' => 'form-control'),
            ))
            ->add('usuarios', ChoiceType::class, array(
                'attr'=>array('class'=>'form-control'),
                'mapped' => false,
                'choices'  => $us
            ));
    }

    public function getName(){
        return 'insMagatzem';
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class'=>'AppBundle\Entity\insMagatzem',
        );
    }
}
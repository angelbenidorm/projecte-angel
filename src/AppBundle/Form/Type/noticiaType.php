<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class noticiaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder->add('titulo', 'text', array(
                'attr'=>array('class'=>'form-control'))
            )
            ->add('texto', TextareaType::class, array(
            'attr' => array('class' => 'form-control',
                            'id' => 'textarea'),
            ))
            ->add('url', 'text', array(
                    'attr'=>array('class'=>'form-control'))
            )
            ->add('fuente', 'text', array(
                    'attr'=>array('class'=>'form-control'))
            )
            ->add('imagenNoticia', FileType::class,array(
                'label'=> 'Imatge',
                'data_class' => null,
                'mapped'=>false
            ));
    }

    public function getName(){
        return 'noticia';
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class'=>'AppBundle\Entity\noticia',
        );
    }
}
<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class tocaType extends AbstractType
{
    private $em;
    private $id;
    public function __construct($em, $id, $agrupacionsComboBox){
        $this->em=$em;
        $this->id=$id;
        $this->agrupacionsComboBox = $agrupacionsComboBox;
    }
    public function buildForm(FormBuilderInterface $builder, array $options){
        $usu=$this->em->getRepository('AppBundle:usuarios')->find($this->id);
        $instr=$this->em->getRepository('AppBundle:instrumento')->findAll();

        $us=array();
        $ins=array();

        //BUSCA L'USUARI AMB L'ID PASSAT PER PARAMETRE AL CONSTRUCTOR
        $us[$usu->getId()]=$usu->getNombre().' '.$usu->getApellidos();

        //ENPLENA ELS INSTRUMENTS EXISTENTS
        foreach($instr as $instrumento){
            $ins[$instrumento->getId()]=$instrumento->getNombre();
        }

        /*if (!$this->getUser()->comprobaDirector() and !$this->is_granted('ROLE_ADMIN')){

        } else {
            foreach $consultaToca
            if $consultaToca->getIdAgrupacion == perdFocus->getIdAgrupacion
            $ins[$consultaToca->getIdIntrumnento()]=$ConsultaToca->getIdInstrumneto()->getNombre();
        }*/
        $builder
            ->add('id_usuario', ChoiceType::class, array(
                'attr'=>array('class'=>'form-control'),
                'mapped' => false,
                'choices'  => $us
            ))
            ->add('id_instrumento', ChoiceType::class, array(
                'attr'=>array('class'=>'form-control'),
                'mapped' => false,
                'choices'  => $ins
            ))
            ->add('id_agrupacion', ChoiceType::class, array(
                'attr'=>array('class'=>'form-control'),
                'mapped' => false,
                'choices'  => $this->agrupacionsComboBox
            ))
            ->add('capDeCorda',CheckboxType::class, array(
                    'label'    => 'És cap de corda?',
                    'required' => false,
                    'mapped'=>false)
            );

    }

    public function getName(){
        return 'toca';
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class'=>'AppBundle\Entity\toca',
        );
    }
}
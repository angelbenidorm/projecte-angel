<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * partitura
 *
 * @ORM\Table(name="partitura")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\partituraRepository")
 */
class partitura
{

    //RELACIO MUCHOS A UNO OBRA
    /**
     * @ORM\ManyToOne(targetEntity="obra")
     */
    private $obra;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="instrumento", type="string", length=255, nullable=true)
     */
    private $instrumento;

    /**
     * @var string
     *
     * @ORM\Column(name="voz", type="string", length=255, nullable=true)
     */
    private $voz;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *  message="Selecciona un PDF"
     * )
     * @Assert\File(
     *     mimeTypes = {"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "Sols s'admeten arxius pdf")
     * @ORM\Column(name="pdf", type="string", length=255, nullable=true)
     */
    private $pdf;

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return partitura
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set instrumento
     *
     * @param string $instrumento
     * @return partitura
     */
    public function setInstrumento($instrumento)
    {
        $this->instrumento = $instrumento;

        return $this;
    }

    /**
     * Get instrumento
     *
     * @return string 
     */
    public function getInstrumento()
    {
        return $this->instrumento;
    }

    /**
     * Set voz
     *
     * @param string $voz
     * @return partitura
     */
    public function setVoz($voz)
    {
        $this->voz = $voz;

        return $this;
    }

    /**
     * Get voz
     *
     * @return string 
     */
    public function getVoz()
    {
        return $this->voz;
    }

    /**
     * Set obra
     *
     * @param \AppBundle\Entity\obra $obra
     * @return partitura
     */
    public function setObra(\AppBundle\Entity\obra $obra = null)
    {
        $this->obra = $obra;

        return $this;
    }

    /**
     * Get obra
     *
     * @return \AppBundle\Entity\obra 
     */
    public function getObra()
    {
        return $this->obra;
    }

}

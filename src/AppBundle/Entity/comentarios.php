<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * comentarios
 *
 * @ORM\Table(name="comentarios")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\comentariosRepository")
 */
class comentarios
{
    //RELACIO MUCHOS A UNO USUARIOS
    /**
     * @ORM\ManyToOne(targetEntity="usuarios")
     */
    private $usuarios;
    //RELACIO MUCHOS A UNO USUARIOS
    /**
     * @ORM\ManyToOne(targetEntity="usuarios")
     */
    private $lastUserModify;
    //RELACIO MUCHOS A UNO EVENTO
    /**
     * @ORM\ManyToOne(targetEntity="evento")
     */
    private $evento;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="string", length=800)
     * @Assert\NotBlank(
     *  message="El text del messatge no pot estar buit."
     * )
     */
    private $texto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastModify", type="datetime", nullable=true)
     */
    private $lastModify;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return comentarios
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return comentarios
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @return \DateTime
     */
    public function getLastModify()
    {
        return $this->lastModify;
    }

    /**
     * @param \DateTime $lastModify
     */
    public function setLastModify($lastModify)
    {
        $this->lastModify = $lastModify;
    }

    /**
     * @return mixed
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * @param mixed $evento
     */
    public function setEvento($evento)
    {
        $this->evento = $evento;
    }

    /**
     * @return mixed
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /**
     * @param mixed $usuarios
     */
    public function setUsuarios($usuarios)
    {
        $this->usuarios = $usuarios;
    }

    /**
     * @return mixed
     */
    public function getLastUserModify()
    {
        return $this->lastUserModify;
    }

    /**
     * @param mixed $lastUserModify
     */
    public function setLastUserModify($lastUserModify)
    {
        $this->lastUserModify = $lastUserModify;
    }


}

<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * agrupacion
 *
 * @ORM\Table(name="agrupacion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\agrupacionRepository")
 */
class agrupacion
{
    //RELACION UNO A MUCHOS EVENTO
    /**
     * @ORM\OneToMany(targetEntity="evento", mappedBy="agrupacion")
     */
    private $evento;

    //RELACION UNO A MUCHOS TOCA
    /**
     * @ORM\OneToMany(targetEntity="toca", mappedBy="idAgrupacion")
     */
    private $toca;
    public function __construct()
    {
        $this->toca=new ArrayCollection();
        $this->evento = new ArrayCollection();
    }




    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(
     *  message="El Nom de l'agrupació no pot estar Buit"
     * )
     */
    private $nombre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return agrupacion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set usuarios
     *
     * @param \AppBundle\Entity\usuarios $usuarios
     * @return agrupacion
     */
    public function setUsuarios(\AppBundle\Entity\usuarios $usuarios = null)
    {
        $this->usuarios = $usuarios;

        return $this;
    }

    /**
     * Get usuarios
     *
     * @return \AppBundle\Entity\usuarios 
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /**
     * Add evento
     *
     * @param \AppBundle\Entity\evento $evento
     * @return agrupacion
     */
    public function addEvento(\AppBundle\Entity\evento $evento)
    {
        $this->evento[] = $evento;

        return $this;
    }

    /**
     * Remove evento
     *
     * @param \AppBundle\Entity\evento $evento
     */
    public function removeConcierto(\AppBundle\Entity\evento $evento)
    {
        $this->evento->removeElement($evento);
    }

    /**
     * Get evento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvento()
    {
        return $this->evento;
    }
}

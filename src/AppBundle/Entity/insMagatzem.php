<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * insMagatzem
 *
 * @ORM\Table(name="insMagatzem")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\insMagatzemRepository")
 */
class insMagatzem
{

    //RELACIO MUCHOS A UNO USUARIOS
    /**
     * @ORM\ManyToOne(targetEntity="usuarios")
     */
    private $usuarios;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(
     *  message="El Nom de l'instrument no pot estar Buit"
     * )
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="numSerie", type="string", length=255, nullable=true)
     * @Assert\NotBlank(
     *  message="El Numero de serie no pot estar Buit"
     * )
     */
    private $numSerie;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     * @Assert\NotBlank(
     *  message="L'estat no pot estar Buit"
     * )
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="observaciones", type="string", length=255, nullable=true)
     */
    private $observaciones;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return insMagatzem
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set numSerie
     *
     * @param string $numSerie
     * @return insMagatzem
     */
    public function setNumSerie($numSerie)
    {
        $this->numSerie = $numSerie;

        return $this;
    }

    /**
     * Get numSerie
     *
     * @return string 
     */
    public function getNumSerie()
    {
        return $this->numSerie;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return insMagatzem
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return insMagatzem
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set usuarios
     *
     * @param \AppBundle\Entity\usuarios $usuarios
     * @return insMagatzem
     */
    public function setUsuarios(\AppBundle\Entity\usuarios $usuarios = null)
    {
        $this->usuarios = $usuarios;

        return $this;
    }

    /**
     * Get usuarios
     *
     * @return \AppBundle\Entity\usuarios 
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }
}

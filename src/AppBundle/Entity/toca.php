<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * toca
 *
 * @ORM\Table(name="toca")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\tocaRepository")
 */
class toca
{


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="id_usuario", type="string", length=255)
     */
    //RELACIO MUCHOS A UNO USUARIS
    /**
     * @ORM\ManyToOne(targetEntity="usuarios")
     */
    private $idUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="id_instrumento", type="string", length=255)
     */
    //RELACIO MUCHOS A UNO instrumento
    /**
     * @ORM\ManyToOne(targetEntity="instrumento")
     */
    private $idInstrumento;

    /**
     * @var string
     *
     * @ORM\Column(name="id_agrupacion", type="string", length=255)
     */
    //RELACIO MUCHOS A UNO AGRUPACIO
    /**
     * @ORM\ManyToOne(targetEntity="agrupacion")
     */
    private $idAgrupacion;

    /**
     * @var boolean $capDeCorda
     * @ORM\Column(name="capDeCorda", type="string", length=2, nullable=true)
     */
    private $capDeCorda;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUsuario
     *
     * @param string $idUsuario
     * @return toca
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return string 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set idInstrumento
     *
     * @param string $idInstrumento
     * @return toca
     */
    public function setIdInstrumento($idInstrumento)
    {
        $this->idInstrumento = $idInstrumento;

        return $this;
    }

    /**
     * Get idInstrumento
     *
     * @return string 
     */
    public function getIdInstrumento()
    {
        return $this->idInstrumento;
    }

    /**
     * Set idAgrupacion
     *
     * @param string $idAgrupacion
     * @return toca
     */
    public function setIdAgrupacion($idAgrupacion)
    {
        $this->idAgrupacion = $idAgrupacion;

        return $this;
    }

    /**
     * Get idAgrupacion
     *
     * @return string 
     */
    public function getIdAgrupacion()
    {
        return $this->idAgrupacion;
    }

    /**
     * @return string
     */
    public function getCapDeCorda()
    {
        return $this->capDeCorda;
    }

    /**
     * @param string $capDeCorda
     */
    public function setCapDeCorda($capDeCorda)
    {
        $this->capDeCorda = $capDeCorda;
    }


}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * asiste
 *
 * @ORM\Table(name="asiste")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\asisteRepository")
 */
class asiste
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_id", type="string", length=255)
     */
    //RELACIO MUCHOS A UNO USUARIS
    /**
     * @ORM\ManyToOne(targetEntity="usuarios")
     */
    private $usuarioId;

    /**
     * @var string
     *
     * @ORM\Column(name="evento_id", type="string", length=255)
     */
    //RELACIO MUCHOS A UNO USUARIS
    /**
     * @ORM\ManyToOne(targetEntity="evento")
     */
    private $eventoId;


    /**
     * @var string
     *
     * @ORM\Column(name="asiste", type="string", length=255)
     */
    private $asiste;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuarioId
     *
     * @param string $usuarioId
     * @return asiste
     */
    public function setUsuarioId($usuarioId)
    {
        $this->usuarioId = $usuarioId;

        return $this;
    }

    /**
     * Get usuarioId
     *
     * @return string 
     */
    public function getUsuarioId()
    {
        return $this->usuarioId;
    }

    /**
     * Set eventoId
     *
     * @param string $eventoId
     * @return asiste
     */
    public function setEventoId($eventoId)
    {
        $this->eventoId = $eventoId;

        return $this;
    }

    /**
     * Get eventoId
     *
     * @return string 
     */
    public function getEventoId()
    {
        return $this->eventoId;
    }



    /**
     * Set asistePorConfirmar
     *
     * @param string $asiste
     * @return asiste
     */
    public function setAsiste($asiste)
    {
        $this->asiste = $asiste;

        return $this;
    }

    /**
     * Get asiste
     *
     * @return string 
     */
    public function getAsiste()
    {
        return $this->asiste;
    }
}

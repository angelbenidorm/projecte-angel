<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\ORM\Mapping as ORM;


use Symfony\Component\Validator\Constraints as Assert;

/**
 * usuarios
 *
 * @ORM\Table(name="usuarios")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\usuariosRepository")
 */
class usuarios implements AdvancedUserInterface
{
    //RELACION UNO A MUCHOS INSTRUMENTO PRESTADO
    /**
     * @ORM\OneToMany(targetEntity="insMagatzem", mappedBy="usuarios")
     */
    private $instrumentoPrestado;

    //RELACION UNO A MUCHOS AGRUPACION
    /**
     * @ORM\OneToMany(targetEntity="agrupacion", mappedBy="usuarios")
     */
    private $agrupacion;

    //RELACION UNO A MUCHOS TOCA
    /**
     * @ORM\OneToMany(targetEntity="toca", mappedBy="idUsuario")
     */
    private $toca;

    //RELACION UNO A MUCHOS ASISTE
    /**
     * @ORM\OneToMany(targetEntity="asiste", mappedBy="usuarioId")
     */
    private $asiste;

    //RELACION UNO A MUCHOS NOTICIA
    /**
     * @ORM\OneToMany(targetEntity="noticia", mappedBy="usuarios")
     */
    private $noticia;

    //RELACION UNO A MUCHOS COMENTARIOS
    /**
     * @ORM\OneToMany(targetEntity="comentarios", mappedBy="usuarios")
     */
    private $comentarios;

    public function __construct()
    {
        $this->asiste= new ArrayCollection();
        $this->toca= new ArrayCollection();
        $this->instrumentoPrestado = new ArrayCollection();
        $this->agrupacion = new ArrayCollection();
        $this->noticia = new ArrayCollection();
        $this->comentarios = new ArrayCollection();
    }




    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(
     *  message="El Nom no pot estar buit."
     * )
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=255, nullable=true)
     * @Assert\NotBlank(
     *  message="El Cognom no pot estar buit."
     * )
     *
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     * @Assert\NotBlank(
     *  message="El Username no pot estar buit."
     * )
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
     * @Assert\Length(
     *  max = "9",
     *  min = "9",
     *  exactMessage="El telèfon ha de ser de 9 dígits."
     * )
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="rol", type="string", length=255)
     */
    private $rol;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenPerfil", type="string", length=255, nullable=true)
     */
    private $imagenPerfil;

    /**
     * @var string $baixa
     * @ORM\Column(name="baixa", type="string", length=2, nullable=true)
     */
    private $baixa;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return usuarios
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return usuarios
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return usuarios
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return usuarios
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return usuarios
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set rol
     *
     * @param string $rol
     * @return usuarios
     */
    public function setRol($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * Get rol
     *
     * @return string 
     */
    public function getRol()
    {
        return $this->rol;
    }


    /**
     * Set imagenPerfil
     *
     * @param string $imagenPerfil
     * @return usuarios
     */
    public function setImagenPerfil($imagenPerfil)
    {
        $this->imagenPerfil = $imagenPerfil;

        return $this;
    }

    /**
     * Get imagenPerfil
     *
     * @return string
     */
    public function getImagenPerfil()
    {
        return $this->imagenPerfil;
    }

    /**
     * @return string
     */
    public function getBaixa()
    {
        return $this->baixa;
    }

    /**
     * @param string $baixa
     */
    public function setBaixa($baixa)
    {
        $this->baixa = $baixa;
    }

    /**
     * Add instrumentoPrestado
     *
     * @param \AppBundle\Entity\insMagatzem $instrumentoPrestado
     * @return usuarios
     */
    public function addInstrumentoPrestado(\AppBundle\Entity\insMagatzem $instrumentoPrestado)
    {
        $this->instrumentoPrestado[] = $instrumentoPrestado;

        return $this;
    }

    /**
     * Remove instrumentoPrestado
     *
     * @param \AppBundle\Entity\insMagatzem $instrumentoPrestado
     */
    public function removeInstrumentoPrestado(\AppBundle\Entity\insMagatzem $instrumentoPrestado)
    {
        $this->instrumentoPrestado->removeElement($instrumentoPrestado);
    }

    /**
     * Get instrumentoPrestado
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInstrumentoPrestado()
    {
        return $this->instrumentoPrestado;
    }

    /**
     * Add agrupacion
     *
     * @param \AppBundle\Entity\agrupacion $agrupacion
     * @return usuarios
     */
    public function addAgrupacion(\AppBundle\Entity\agrupacion $agrupacion)
    {
        $this->agrupacion[] = $agrupacion;

        return $this;
    }

    /**
     * Remove agrupacion
     *
     * @param \AppBundle\Entity\agrupacion $agrupacion
     */
    public function removeAgrupacion(\AppBundle\Entity\agrupacion $agrupacion)
    {
        $this->agrupacion->removeElement($agrupacion);
    }

    /**
     * Get agrupacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAgrupacion()
    {
        return $this->agrupacion;
    }

    /**
     * Add evento
     *
     * @param \AppBundle\Entity\evento $evento
     * @return usuarios
     */
    public function addEvento(\AppBundle\Entity\evento $evento)
    {
        $this->evento[] = $evento;

        return $this;
    }

    /**
     * Remove evento
     *
     * @param \AppBundle\Entity\evento $evento
     */
    public function removeEvento(\AppBundle\Entity\evento $evento)
    {
        $this->evento->removeElement($evento);
    }

    /**
     * Get evento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * @return mixed
     */
    public function getToca()
    {
        return $this->toca;
    }


    public function comprobarDirector(){
        if (!in_array('ROLE_MUSIC', $this->getRoles())){
            return false;
        }
        $toca = $this->getToca();

        $instrum = array();
        foreach($toca as $result){
            array_push($instrum, $result->getIdInstrumento()->getNombre());

            if (in_array('Director', $instrum)){
                return true;
            }
        }
        return false;
    }

    public function esCapDeCorda(){
        if (!in_array('ROLE_MUSIC', $this->getRoles())){
            return false;
        }
        $toca = $this->getToca();
        foreach($toca as $result){
            if ($result->getCapDeCorda() == 'Sí'){
                return true;
            }
            return false;
        }
    }


//METODES SOBREESCRITS DE LA INTERFACE
    public function eraseCredentials()
    { }
    function equals(UserInterface $user)
    {
        return $user->getUsername() === $this->username;
    }
    public function getRoles()
    {
        return explode(',', $this->rol);
    }
    public function isAccountNonExpired()
    {
        return true;
    }
    public function isAccountNonLocked()
    {
        return true;
    }
    public function isCredentialsNonExpired()
    {
        return true;
    }
    public function getIsActive(){
        return true;
    }
    public function isEnabled()
    {
        return $this->getIsActive();
    }
    public function getSalt(){
    }


}

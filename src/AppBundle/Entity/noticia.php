<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * noticia
 *
 * @ORM\Table(name="noticia")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\noticiaRepository")
 */
class noticia
{
    //RELACIO MUCHOS A UNO USUARIOS
    /**
     * @ORM\ManyToOne(targetEntity="usuarios")
     */
    private $usuarios;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=100)
     * @Assert\NotBlank(
     *  message="El títol no pot estar buit."
     * )
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="string", length=1500)
     * @Assert\NotBlank(
     *  message="La noticia no pot estar buida."
     * )
     */
    private $texto;

    /**
     * @var string
     *
     * @ORM\Column(name="fuente", type="string", length=40, nullable=true)
     */
    private $fuente;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenNoticia", type="string", length=255, nullable=true)
     */
    private $imagenNoticia;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return noticia
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return noticia
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set fuente
     *
     * @param string $fuente
     * @return noticia
     */
    public function setFuente($fuente)
    {
        $this->fuente = $fuente;

        return $this;
    }

    /**
     * Get fuente
     *
     * @return string 
     */
    public function getFuente()
    {
        return $this->fuente;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return noticia
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return noticia
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getImagenNoticia()
    {
        return $this->imagenNoticia;
    }

    /**
     * @param string $imagenNoticia
     */
    public function setImagenNoticia($imagenNoticia)
    {
        $this->imagenNoticia = $imagenNoticia;
    }

    /**
     * Set usuarios
     *
     * @param \AppBundle\Entity\usuarios $usuarios
     * @return noticia
     */
    public function setUsuarios(\AppBundle\Entity\usuarios $usuarios = null)
    {
        $this->usuarios = $usuarios;

        return $this;
    }

    /**
     * Get usuarios
     *
     * @return \AppBundle\Entity\usuarios
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }
}

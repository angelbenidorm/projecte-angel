<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * evento
 *
 * @ORM\Table(name="evento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\eventoRepository")
 */
class evento
{

    //RELACION MUCHOS A UNO AGRUPACION
    /**
     * @ORM\ManyToOne(targetEntity="agrupacion")
     */
    private $agrupacion;
    //RELACION MUCHOS A MUCHOS USUARIOS INTERPRETA OBRA CONCIERTO
    // atencio, no pots borrar una obra si está afegida a un concert....
    // si es gasta el delete on cascade si borres l'obra es borren tots els concerts on está l'obra i tots els concerts
    /**
     * @ORM\ManyToMany(targetEntity="obra", inversedBy="evento")
     */
    private $obra;

    //RELACION UNO A MUCHOS ASISTE
    /**
     * @ORM\OneToMany(targetEntity="asiste", mappedBy="eventoId")
     */
    private $asiste;

    //RELACION UNO A MUCHOS COMENTARIOS
    /**
     * @ORM\OneToMany(targetEntity="comentarios", mappedBy="evento")
     */
    private $comentarios;

    public function __construct()
    {
        $this->asiste = new ArrayCollection();
        $this->obra = new ArrayCollection();
        $this->comentarios = new ArrayCollection();
    }



    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *  message="El Nom no pot estar Buit"
     * )
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(name="fechaHora", type="datetime", nullable=true)
     */
    private $fechaHora;

    /**
     * @var string
     *
     * @ORM\Column(name="lugar", type="string", length=255, nullable=true)
     * @Assert\NotBlank(
     *  message="El Lloc no pot estar buit"
     * )
     */
    private $lugar;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255, nullable=true)
     */
    private $tipo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return evento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaHora
     *
     * @param \DateTime $fechaHora
     * @return evento
     */
    public function setFechaHora($fechaHora)
    {
        $this->fechaHora = $fechaHora;

        return $this;
    }

    /**
     * Get fechaHora
     *
     * @return \DateTime 
     */
    public function getFechaHora()
    {
        return $this->fechaHora;
    }

    /**
     * Set lugar
     *
     * @param string $lugar
     * @return evento
     */
    public function setLugar($lugar)
    {
        $this->lugar = $lugar;

        return $this;
    }

    /**
     * Get lugar
     *
     * @return string 
     */
    public function getLugar()
    {
        return $this->lugar;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return evento
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Add usuarios
     *
     * @param \AppBundle\Entity\usuarios $usuarios
     * @return evento
     */
    public function addUsuario(\AppBundle\Entity\usuarios $usuarios)
    {
        $this->usuarios[] = $usuarios;

        return $this;
    }

    /**
     * Remove usuarios
     *
     * @param \AppBundle\Entity\usuarios $usuarios
     */
    public function removeUsuario(\AppBundle\Entity\usuarios $usuarios)
    {
        $this->usuarios->removeElement($usuarios);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /**
     * Set agrupacion
     *
     * @param \AppBundle\Entity\agrupacion $agrupacion
     * @return evento
     */
    public function setAgrupacion(\AppBundle\Entity\agrupacion $agrupacion = null)
    {
        $this->agrupacion = $agrupacion;

        return $this;
    }

    /**
     * Get agrupacion
     *
     * @return \AppBundle\Entity\agrupacion 
     */
    public function getAgrupacion()
    {
        return $this->agrupacion;
    }

    /**
     * Add obra
     *
     * @param \AppBundle\Entity\obra $obra
     * @return evento
     */
    public function addObra(\AppBundle\Entity\obra $obra)
    {
        $this->obra[] = $obra;

        return $this;
    }

    /**
     * Remove obra
     *
     * @param \AppBundle\Entity\obra $obra
     */
    public function removeObra(\AppBundle\Entity\obra $obra)
    {
        $this->obra->removeElement($obra);
    }

    /**
     * Get obra
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getObra()
    {
        return $this->obra;
    }

    /**
     * @return mixed
     */
    public function getAsiste()
    {
        return $this->asiste;
    }

    /**
     * @param mixed $asiste
     */
    public function setAsiste($asiste)
    {
        $this->asiste = $asiste;
    }

    /**
     * @return mixed
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * @param mixed $comentarios
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;
    }


}

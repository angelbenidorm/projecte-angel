<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * obra
 *
 * @ORM\Table(name="obra")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\obraRepository")
 */
class obra
{
    //RELACION UNO A MUCHOS PARTITURA
    /**
     * @ORM\OneToMany(targetEntity="partitura", mappedBy="obra", cascade={"remove"})
     */
    private $partitura;
    //RELACION MUCHOS A MUCHOS CONCIERTO
    /**
     * @ORM\ManyToMany(targetEntity="evento", mappedBy="obra",cascade={"remove"})
     */
    private $evento;
    public function __construct()
    {
        $this->partitura = new ArrayCollection();
        $this->evento = new ArrayCollection();
    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     *
     * @Assert\NotBlank(
     *  message="El Nom no pot estar Buit"
     * )
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="compositor", type="string", length=255, nullable=true)
     * @Assert\NotBlank(
     *  message="El Compositor no pot estar Buit"
     * )
     */
    private $compositor;

    /**
     * @var string
     *
     * @ORM\Column(name="genero", type="string", length=255, nullable=true)
     */
    private $genero;

    /**
     * @var string
     *
     * @ORM\Column(name="arreglista", type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(
     *  message="El Arreglista no pot estar Buit"
     * )
     */
    private $arreglista;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=800, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", length=255, nullable=true)
     */
    private $video;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return obra
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set compositor
     *
     * @param string $compositor
     * @return obra
     */
    public function setCompositor($compositor)
    {
        $this->compositor = $compositor;

        return $this;
    }

    /**
     * Get compositor
     *
     * @return string 
     */
    public function getCompositor()
    {
        return $this->compositor;
    }

    /**
     * Set genero
     *
     * @param string $genero
     * @return obra
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string 
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set arreglista
     *
     * @param string $arreglista
     * @return obra
     */
    public function setArreglista($arreglista)
    {
        $this->arreglista = $arreglista;

        return $this;
    }

    /**
     * Get arreglista
     *
     * @return string 
     */
    public function getArreglista()
    {
        return $this->arreglista;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * @param string $video
     */
    public function setVideo($video)
    {
        $this->video = $video;
    }

    /**
     * Add partitura
     *
     * @param \AppBundle\Entity\agrupacion $partitura
     * @return obra
     */
    public function addPartitura(\AppBundle\Entity\agrupacion $partitura)
    {
        $this->partitura[] = $partitura;

        return $this;
    }

    /**
     * Remove partitura
     *
     * @param \AppBundle\Entity\agrupacion $partitura
     */
    public function removePartitura(\AppBundle\Entity\agrupacion $partitura)
    {
        $this->partitura->removeElement($partitura);
    }

    /**
     * Get partitura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPartitura()
    {
        return $this->partitura;
    }
}
